﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnifiApp
{
    /// <summary>
    /// Interaction logic for AddNewSite.xaml
    /// </summary>

    public partial class AddNewSite : Page
    {
        public string adminName = "Admin[koitadmin]";
        static List<JsonSites> sitesList = new List<JsonSites>();
        static List<JsonAlarmsList> sitesAlarmsList = new List<JsonAlarmsList>();
        static List<JsonEventsList> sitesEventsList = new List<JsonEventsList>();
        static List<JsonWlanGroups> sitesWlanGroupList = new List<JsonWlanGroups>();
        static List<JsonSitesHealth> sitesHealthList = new List<JsonSitesHealth>();
        static List<JsonSitesDevicesList> sitesOverviewList = new List<JsonSitesDevicesList>();



        const String randomStringGenerator = "abcdefghijklmnopqrstuvwxyz0123456789";
        const String randomTimeNumberGenerator = "123456789";
        private static Random random = new Random((int)DateTime.Now.Ticks);
        private static Random dateGenerator = new Random();
        public AddNewSite()
        {
            InitializeComponent();
            ShowsNavigationUI = true;
            AddNewSiteJsonTextBox.Visibility = Visibility.Hidden;
        }
        private static DateTime ThisMonth()
        {
            var thisMonth = DateTime.Now.Month;
            DateTime start = new DateTime(2018, thisMonth, 1, dateGenerator.Next(0, 24), dateGenerator.Next(0, 60), dateGenerator.Next(0, 60), 0);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(dateGenerator.Next(range));
        }
        private static DateTime ThisToday()
        {
            var thisToday = DateTime.Now.Day;
            var thisMonth = DateTime.Now.Month;
            var today = DateTime.Now;
            DateTime start = new DateTime(2018, thisMonth, thisToday, dateGenerator.Next(0, 24), dateGenerator.Next(0, 60), dateGenerator.Next(0, 60), 0);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(dateGenerator.Next(range));
        }
        private static DateTime ThisTodayLastHour()
        {
            var thisMinutes = DateTime.Now.Minute;
            var thisHour = DateTime.Now.Hour;
            var thisToday = DateTime.Now.Day;
            var thisMonth = DateTime.Now.Month;
            var today = DateTime.Now;
            DateTime start = new DateTime(2018, thisMonth, thisToday, thisHour, thisMinutes, dateGenerator.Next(0, 60), 0);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(dateGenerator.Next(range));

        }
        private static DateTime RandomDay()
        {
            DateTime start = new DateTime(2018, 1, 1, dateGenerator.Next(0, 24), dateGenerator.Next(0, 60), dateGenerator.Next(0, 60), 0);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(dateGenerator.Next(range));
        }
        private static int RandomNumber(int maxNumber)
        {
            Random random = new Random();
            return random.Next(0, maxNumber);
        }
        public string GetRandomIpAddress()
        {
            var random = new Random();
            return $"{random.Next(1, 255)}.{random.Next(0, 255)}.{random.Next(0, 255)}.{random.Next(0, 255)}";
        }

        private void AddNewSiteSubmitButton_Click(object sender, RoutedEventArgs e)
        {
            if (AddNewSiteTextBox.Text == "")
            {
                MessageBox.Show("Please add site name!");
                //MessageBox.Show(RandomDay().ToString());
            }
            else
            {

                var filePathSites = @"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSites.json";
                var filePathAlarms = @"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonAlarms.json";
                var filePathWlanGroups = @"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonWlanGroups.json";
                var filePathEvents = @"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonEvents.json";
                var filePathSiteHealth = @"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSitesHealth.json";
                var filePathSiteOverview = @"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSitesDevices.json";
                // Read existing json data
                sitesList = JsonConvert.DeserializeObject<List<JsonSites>>(System.IO.File.ReadAllText(filePathSites));
                sitesAlarmsList = JsonConvert.DeserializeObject<List<JsonAlarmsList>>(System.IO.File.ReadAllText(filePathAlarms));
                sitesEventsList = JsonConvert.DeserializeObject<List<JsonEventsList>>(System.IO.File.ReadAllText(filePathEvents));
                sitesWlanGroupList = JsonConvert.DeserializeObject<List<JsonWlanGroups>>(System.IO.File.ReadAllText(filePathWlanGroups));
                sitesHealthList = JsonConvert.DeserializeObject<List<JsonSitesHealth>>(System.IO.File.ReadAllText(filePathSiteHealth));
                sitesOverviewList = JsonConvert.DeserializeObject<List<JsonSitesDevicesList>>(System.IO.File.ReadAllText(filePathSiteOverview));
                string siteIdGeneration = RandomStringGenerator(15);
                string currentIp = GetRandomIpAddress();


                JsonSites newJsonSites = new JsonSites
                {
                    _id = siteIdGeneration,
                    desc = AddNewSiteTextBox.Text,
                    name = RandomStringGenerator(10),
                    role = "admin",

                };
                sitesList.Add(newJsonSites);


                // Update json data string
                String jsonSites = JsonConvert.SerializeObject(sitesList, Formatting.Indented);

                System.IO.File.WriteAllText(filePathSites, jsonSites);
                string correctJson = File.ReadAllText(filePathSites);
                //MessageBox.Show(correctJson);


                sitesAlarmsList = JsonConvert.DeserializeObject<List<JsonAlarmsList>>(System.IO.File.ReadAllText(filePathAlarms));
                string macAddress = RandomStringGenerator(2) + ":" + RandomStringGenerator(2) + ":" + RandomStringGenerator(2) + ":" + RandomStringGenerator(2) + ":" + RandomStringGenerator(2) + ":" + RandomStringGenerator(2);

                JsonAlarmsList jsonAlarmsList = new JsonAlarmsList
                {

                    _id = RandomStringGenerator(20),
                    ap = macAddress,
                    ap_name = macAddress,
                    archived = false,
                    datetime = System.DateTime.Today,
                    key = "EVT_AP_Lost_Contact",
                    msg = "AP[" + macAddress + "]" + "was disconnected",
                    site_id = siteIdGeneration,
                    subsystem = "wlan",
                    time = RandomTimeGenerator(13),

                };
                JsonAlarmsList jsonAlarmsList2 = new JsonAlarmsList
                {

                    _id = RandomStringGenerator(20),
                    ap = macAddress,
                    ap_name = macAddress,
                    archived = false,
                    datetime = ThisMonth(),
                    key = "EVT_AP_Lost_Contact",
                    msg = "AP[" + macAddress + "]" + "was disconnected",
                    site_id = siteIdGeneration,
                    subsystem = "wlan",
                    time = RandomTimeGenerator(13),

                };
                JsonAlarmsList jsonAlarmsList3 = new JsonAlarmsList
                {

                    _id = RandomStringGenerator(20),
                    ap = macAddress,
                    ap_name = macAddress,
                    archived = false,
                    datetime = System.DateTime.Now.AddHours(-0.5),
                    key = "EVT_AP_Lost_Contact",
                    msg = "AP[" + macAddress + "]" + "was disconnected",
                    site_id = siteIdGeneration,
                    subsystem = "wlan",
                    time = RandomTimeGenerator(13),

                };
                JsonAlarmsList jsonAlarmsList4 = new JsonAlarmsList
                {

                    _id = RandomStringGenerator(20),
                    ap = macAddress,
                    ap_name = macAddress,
                    archived = false,
                    datetime = RandomDay(),
                    key = "EVT_AP_Lost_Contact",
                    msg = "AP[" + macAddress + "]" + "was disconnected",
                    site_id = siteIdGeneration,
                    subsystem = "wlan",
                    time = RandomTimeGenerator(13),

                };
                JsonAlarmsList jsonAlarmsList5 = new JsonAlarmsList
                {

                    _id = RandomStringGenerator(20),
                    ap = macAddress,
                    ap_name = macAddress,
                    archived = false,
                    datetime = RandomDay(),
                    key = "EVT_AP_Lost_Contact",
                    msg = "AP[" + macAddress + "]" + "was disconnected",
                    site_id = siteIdGeneration,
                    subsystem = "wlan",
                    time = RandomTimeGenerator(13),

                };
                JsonEventsList jsonEventsList = new JsonEventsList
                {
                    _id = RandomStringGenerator(20),
                    admin = adminName,
                    datetime = System.DateTime.Today,
                    ip = currentIp,
                    is_admin = true,
                    key = "EVT_AD_Login",
                    msg = adminName + "log in from " + currentIp,
                    subsystem = "wlan",
                    time = RandomTimeGenerator(13),
                    site_id = siteIdGeneration
                };
                JsonEventsList jsonEventsList2 = new JsonEventsList
                {
                    _id = RandomStringGenerator(20),
                    admin = adminName,
                    datetime = ThisMonth(),
                    ip = currentIp,
                    is_admin = true,
                    key = "EVT_AD_Login",
                    msg = adminName + "log in from " + currentIp,
                    subsystem = "wlan",
                    time = RandomTimeGenerator(13),
                    site_id = siteIdGeneration
                };
                JsonEventsList jsonEventsList3 = new JsonEventsList
                {
                    _id = RandomStringGenerator(20),
                    admin = adminName,
                    datetime = System.DateTime.Now.AddHours(-0.5),
                    ip = currentIp,
                    is_admin = true,
                    key = "EVT_AD_Login",
                    msg = adminName + "log in from " + currentIp,
                    subsystem = "wlan",
                    time = RandomTimeGenerator(13),
                    site_id = siteIdGeneration
                };
                JsonEventsList jsonEventsList4 = new JsonEventsList
                {
                    _id = RandomStringGenerator(20),
                    admin = adminName,
                    datetime = RandomDay(),
                    ip = currentIp,
                    is_admin = true,
                    key = "EVT_AD_Login",
                    msg = adminName + "log in from " + currentIp,
                    subsystem = "wlan",
                    time = RandomTimeGenerator(13),
                    site_id = siteIdGeneration
                };
                JsonEventsList jsonEventsList5 = new JsonEventsList
                {
                    _id = RandomStringGenerator(20),
                    admin = adminName,
                    datetime = RandomDay(),
                    ip = currentIp,
                    is_admin = true,
                    key = "EVT_AD_Login",
                    msg = adminName + "log in from " + currentIp,
                    subsystem = "wlan",
                    time = RandomTimeGenerator(13),
                    site_id = siteIdGeneration
                };
                JsonWlanGroups jsonWlanGroups = new JsonWlanGroups
                {
                    _id = RandomStringGenerator(20),
                    attr_hidden = false,
                    attr_hidden_id = false.ToString(),
                    attr_no_delete = true,
                    attr_no_edit = true,
                    name = RandomStringGenerator(10),
                    site_id = siteIdGeneration

                };
                JsonWlanGroups jsonWlanGroups2 = new JsonWlanGroups
                {
                    _id = RandomStringGenerator(20),
                    attr_hidden = false,
                    attr_hidden_id = false.ToString(),
                    attr_no_delete = true,
                    attr_no_edit = true,
                    name = RandomStringGenerator(10),
                    site_id = siteIdGeneration

                };

                JsonSitesHealth jsonSitesHealth = new JsonSitesHealth
                {
                    _id = siteIdGeneration,
                    desc = AddNewSiteTextBox.Text,
                    num_new_alarms = 5,
                    healthLan = new Health
                    {

                        subsystem = "lan",
                        num_ap = RandomNumber(50),
                        num_pending = RandomNumber(25)

                    },

                    healthWlan = new Health
                    {
                        subsystem = "wlan",
                        num_ap = RandomNumber(50),
                        num_disconnected = RandomNumber(50),
                        num_pending = RandomNumber(25),
                        num_user = RandomNumber(25),
                        num_guest = RandomNumber(25),
                        status = "ok"
                    }

                };
                JsonSitesDevicesList jsonSitesDevicesList = new JsonSitesDevicesList
                {
                    _id = RandomStringGenerator(20),
                    ip = currentIp,
                    name = RandomStringGenerator(20),
                    site_id = siteIdGeneration,
                    ethernet_table = new JsonSitesDevicesList.Ethernet_Table
                    {
                        mac = macAddress,
                        name = RandomStringGenerator(20),
                        num_port = 1
                    },
                };
                JsonSitesDevicesList jsonSitesDevicesList2 = new JsonSitesDevicesList
                {
                    _id = RandomStringGenerator(20),
                    ip = currentIp,
                    name = RandomStringGenerator(20),
                    site_id = siteIdGeneration,
                    ethernet_table = new JsonSitesDevicesList.Ethernet_Table
                    {
                        mac = macAddress,
                        name = RandomStringGenerator(20),
                        num_port = 1
                    },
                };
                JsonSitesDevicesList jsonSitesDevicesList3 = new JsonSitesDevicesList
                {
                    _id = RandomStringGenerator(20),
                    ip = currentIp,
                    name = RandomStringGenerator(20),
                    site_id = siteIdGeneration,
                    ethernet_table = new JsonSitesDevicesList.Ethernet_Table
                    {
                        mac = macAddress,
                        name = RandomStringGenerator(20),
                        num_port = 1
                    },
                };

                sitesAlarmsList.Add(jsonAlarmsList);
                sitesAlarmsList.Add(jsonAlarmsList2);
                sitesAlarmsList.Add(jsonAlarmsList3);
                sitesAlarmsList.Add(jsonAlarmsList4);
                sitesAlarmsList.Add(jsonAlarmsList5);
                sitesWlanGroupList.Add(jsonWlanGroups);
                sitesWlanGroupList.Add(jsonWlanGroups2);
                sitesEventsList.Add(jsonEventsList);
                sitesEventsList.Add(jsonEventsList2);
                sitesEventsList.Add(jsonEventsList3);
                sitesEventsList.Add(jsonEventsList4);
                sitesEventsList.Add(jsonEventsList5);
                sitesHealthList.Add(jsonSitesHealth);
                sitesOverviewList.Add(jsonSitesDevicesList);
                sitesOverviewList.Add(jsonSitesDevicesList2);
                sitesOverviewList.Add(jsonSitesDevicesList3);





                String jsonAlarms = JsonConvert.SerializeObject(sitesAlarmsList, Formatting.Indented);
                String jsonEvents = JsonConvert.SerializeObject(sitesEventsList, Formatting.Indented);
                String jsonWlanGroupsAdd = JsonConvert.SerializeObject(sitesWlanGroupList, Formatting.Indented);
                String jsonSitesHealthAdd = JsonConvert.SerializeObject(sitesHealthList, Formatting.Indented);
                String jsonSitesOverviewAdd = JsonConvert.SerializeObject(sitesOverviewList, Formatting.Indented);



                File.WriteAllText(filePathAlarms, jsonAlarms);
                File.WriteAllText(filePathWlanGroups, jsonWlanGroupsAdd);
                File.WriteAllText(filePathEvents, jsonEvents);
                File.WriteAllText(filePathSiteHealth, jsonSitesHealthAdd);
                File.WriteAllText(filePathSiteOverview, jsonSitesOverviewAdd);


                MessageBox.Show("Added new site!");

            }

        }
        private static string RandomStringGenerator(int length)
        {
            var randomBuilder = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                var c = randomStringGenerator[random.Next(0, randomStringGenerator.Length)];
                randomBuilder.Append(c);
            }
            return randomBuilder.ToString();
        }
        private static string RandomTimeGenerator(int length)
        {
            var randomBuilder = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                var c = randomTimeNumberGenerator[random.Next(0, randomTimeNumberGenerator.Length)];
                randomBuilder.Append(c);
            }
            return randomBuilder.ToString();
        }


        private void WlanButton_Click(object sender, RoutedEventArgs e)
        {
            string selectedSiteName = AddNewSiteTextBox.Text;
            DataContainer.siteName = selectedSiteName;
            NavigationService.Navigate(new Uri("SiteWlanSetup.xaml", UriKind.Relative));

        }

        private void AllSitesDownload_Click(object sender, RoutedEventArgs e)
        {
            String allSites = File.ReadAllText("testDataJson/JsonSites.json");
            AddNewSiteJsonTextBox.Text = allSites;

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(AddNewSiteJsonTextBox.Text, typeof(List<JsonSites>));

            foreach (JsonSites json in correctSitesJsonList)
            {

            }
        }
    }
}
