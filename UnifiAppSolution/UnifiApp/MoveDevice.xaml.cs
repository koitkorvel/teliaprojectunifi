﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnifiApp
{
    /// <summary>
    /// Interaction logic for MoveDevice.xaml
    /// </summary>
    public partial class MoveDevice : Page
    {
        private string correctDeviceMac = string.Empty;
        private string correctMoveToSiteId = string.Empty;
        private string fromSiteDefaultId = string.Empty;
        static List<JsonSitesDevicesList> sitesMovedDevicesList = new List<JsonSitesDevicesList>();
        static List<JsonSitesDevicesList> sitesMovedDevicesListMoved;
        static List<JsonSitesDevicesList> sitesAddNewDevices = new List<JsonSitesDevicesList>();
        const String randomTimeNumberGenerator = "123456789";
        const String randomStringGenerator = "abcdefghijklmnopqrstuvwxyz0123456789";
        private static Random random = new Random((int)DateTime.Now.Ticks);


        public MoveDevice()
        {
            InitializeComponent();
            JsonDevicesTextBox.Visibility = Visibility.Hidden;
            JsonSitesTextBox.Visibility = Visibility.Hidden;
            AddRandomDeviceButton.Visibility = Visibility.Hidden;
            ShowsNavigationUI = true;
            WebClient webClient = new WebClient();
            string allSites = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSites.json");
            JsonSitesTextBox.Text = allSites;

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonSitesTextBox.Text, typeof(List<JsonSites>));
            List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();

            foreach (JsonSites moveToSites in sortedSitesJsonList)
            {
                MoveToSitesCombobox.Items.Add(moveToSites.desc);
            }

            foreach (JsonSites existingSiteJson in sortedSitesJsonList)
            {
                if (existingSiteJson.desc == "puhverDevice")
                {
                    MoveFromSitesCombobox.Items.Add(existingSiteJson.desc);

                }
                else if (existingSiteJson.desc == "puhverDevice2")
                {
                    MoveFromSitesCombobox.Items.Add(existingSiteJson.desc);
                }

            }
        }


        private void ShowSiteDevicesButton_Click(object sender, RoutedEventArgs e)
        {
            MoveFromSitesDevicesCombobox.Items.Clear();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonSitesTextBox.Text, typeof(List<JsonSites>));
            List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();


            if (MoveFromSitesCombobox.SelectedItem == null)
            {
                MessageBox.Show("Please select site!");
            }
            else
            {

                foreach (JsonSites existingSite in sortedSitesJsonList)
                {

                    string selectedSite = MoveFromSitesCombobox.SelectedItem.ToString();
                    if (selectedSite == existingSite.desc)
                    {
                        string sitesDevicesJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSitesDevices.json");
                        string siteId = existingSite._id;
                        JsonDevicesTextBox.Text = sitesDevicesJson;

                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        List<JsonSitesDevicesList> correctSitesJsonDevicesList = (List<JsonSitesDevicesList>)serializer.Deserialize(JsonDevicesTextBox.Text, typeof(List<JsonSitesDevicesList>));

                        foreach (JsonSitesDevicesList devices in correctSitesJsonDevicesList)
                        {
                            if (devices.site_id == siteId)
                            {
                                MoveFromSitesDevicesCombobox.Items.Add(devices.name);

                            }



                        }

                    }

                }
            }

        }
        private void MoveDeviceSubmitButton_Click(object sender, RoutedEventArgs e)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<JsonSitesDevicesList> correctSitesJsonDevicesList = (List<JsonSitesDevicesList>)serializer.Deserialize(JsonDevicesTextBox.Text, typeof(List<JsonSitesDevicesList>));
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)serializer.Deserialize(JsonSitesTextBox.Text, typeof(List<JsonSites>));
            List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();

            string id = string.Empty;
            string ip = string.Empty;
            string name = string.Empty;
            string siteId = string.Empty;
            string mac = string.Empty;
            string macName = string.Empty;
            int numPort = 0;
            string selectedMoveToSite = MoveToSitesCombobox.SelectedItem.ToString();
            string selectedDevice = MoveFromSitesDevicesCombobox.SelectedItem.ToString();
            string moveToSiteID = string.Empty;

            foreach (JsonSites site in correctSitesJsonList)
            {
                if (selectedMoveToSite == site.desc)
                    moveToSiteID = site._id;

                foreach (JsonSitesDevicesList device in correctSitesJsonDevicesList)
                {
                    if (selectedDevice == device.name)
                    {

                        id = device._id;
                        ip = device.ip;
                        name = device.name;
                        siteId = device.site_id;
                        mac = device.ethernet_table.mac;
                        macName = device.ethernet_table.name;
                        numPort = device.ethernet_table.num_port;


                    }
                }
            }

            //MessageBox.Show(selectedMoveToSite);

            var filePathSiteDevices = @"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\MovedSiteDevices.json";
            var filePathSiteOverview = @"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSitesDevices.json";


            sitesMovedDevicesList = JsonConvert.DeserializeObject<List<JsonSitesDevicesList>>(System.IO.File.ReadAllText(filePathSiteOverview));
            sitesMovedDevicesListMoved = JsonConvert.DeserializeObject<List<JsonSitesDevicesList>>(System.IO.File.ReadAllText(filePathSiteDevices));
            JsonSitesDevicesList jsonSitesDevicesList = new JsonSitesDevicesList
            {
                _id = id,
                ip = ip,
                site_id = moveToSiteID,
                name = name,
                ethernet_table = new JsonSitesDevicesList.Ethernet_Table
                {
                    mac = mac,
                    name = macName,
                    num_port = numPort,
                }


            };

            sitesMovedDevicesList.Add(jsonSitesDevicesList);
            sitesMovedDevicesListMoved.Add(jsonSitesDevicesList);
            String jsonMovedDevicesList = JsonConvert.SerializeObject(sitesMovedDevicesList, Formatting.Indented);
            String jsonMovedDevicesListMoved = JsonConvert.SerializeObject(sitesMovedDevicesListMoved, Formatting.Indented);
            //1 lisamine
            File.WriteAllText(filePathSiteOverview, jsonMovedDevicesList);
            File.WriteAllText(filePathSiteDevices, jsonMovedDevicesListMoved);

            //2 kustutamine
            var removeDeviceAfterAdd = JsonConvert.DeserializeObject<List<JsonSitesDevicesList>>(System.IO.File.ReadAllText(filePathSiteOverview));
            var newJsonString = JsonConvert.SerializeObject(removeDeviceAfterAdd.Where(i => i._id != id));
            File.WriteAllText(filePathSiteOverview, newJsonString);
            AddnewDevices();
            MessageBox.Show("Added");
        }

        public string GetRandomIpAddress()
        {
            var random = new Random();
            return $"{random.Next(1, 255)}.{random.Next(0, 255)}.{random.Next(0, 255)}.{random.Next(0, 255)}";
        }
        private static string RandomStringGenerator(int length)
        {
            var randomBuilder = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                var c = randomStringGenerator[random.Next(0, randomStringGenerator.Length)];
                randomBuilder.Append(c);
            }
            return randomBuilder.ToString();
        }
        private void AddRandomDeviceButton_Click(object sender, RoutedEventArgs e)
        {
            AddnewDevices();

        }

        private void AddnewDevices()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<JsonSitesDevicesList> correctSitesJsonDevicesList = (List<JsonSitesDevicesList>)serializer.Deserialize(JsonDevicesTextBox.Text, typeof(List<JsonSitesDevicesList>));
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)serializer.Deserialize(JsonSitesTextBox.Text, typeof(List<JsonSites>));
            List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();
            string currentIp = GetRandomIpAddress();
            string siteIdGeneration = RandomStringGenerator(15);
            string macAddress = RandomStringGenerator(2) + ":" + RandomStringGenerator(2) + ":" + RandomStringGenerator(2) + ":" + RandomStringGenerator(2) + ":" + RandomStringGenerator(2) + ":" + RandomStringGenerator(2);

            var filePathSiteOverview = @"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSitesDevices.json";
            sitesAddNewDevices = JsonConvert.DeserializeObject<List<JsonSitesDevicesList>>(System.IO.File.ReadAllText(filePathSiteOverview));

            string selectedSite = MoveFromSitesCombobox.SelectedItem.ToString();

            foreach (JsonSites site in sortedSitesJsonList)
            {
                if (selectedSite == site.desc)
                {
                    string selectedSiteId = site._id;
                    DataContainer.selectedSiteIdDevices = selectedSiteId;
                }
            }

            foreach (JsonSites site in correctSitesJsonList)
            {
                if (DataContainer.selectedSiteIdDevices == site._id)
                {
                    JsonSitesDevicesList jsonSitesDevicesList = new JsonSitesDevicesList
                    {
                        _id = RandomStringGenerator(20),
                        ip = currentIp,
                        name = RandomStringGenerator(20),
                        site_id = DataContainer.selectedSiteIdDevices,
                        ethernet_table = new JsonSitesDevicesList.Ethernet_Table
                        {
                            mac = macAddress,
                            name = RandomStringGenerator(20),
                            num_port = 1
                        },
                    };

                    sitesAddNewDevices.Add(jsonSitesDevicesList);
                }
            }
            String jsonSitesOverviewAdd = JsonConvert.SerializeObject(sitesAddNewDevices, Formatting.Indented);
            File.WriteAllText(filePathSiteOverview, jsonSitesOverviewAdd);

        }
    }
}
