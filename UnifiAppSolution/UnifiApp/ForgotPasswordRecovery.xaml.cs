﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevOne.Security.Cryptography.BCrypt;

namespace UnifiApp
{
    /// <summary>
    /// Interaction logic for ForgotPasswordRecovery.xaml
    /// </summary>
    public partial class ForgotPasswordRecovery : Page
    {
        private string username = string.Empty;
        private string password = string.Empty;
        public ForgotPasswordRecovery()
        {
            InitializeComponent();
            ForgotTestConfirmationCode.Text = DataContainer.confirmationCode;
            ForgotTestConfirmationCode.Visibility = Visibility.Hidden;
        }

        private void ForgotChangeUserPasswordButton_Click(object sender, RoutedEventArgs e)
        {
            if (ForgotConfirmationCodeTextbox.Text == ForgotTestConfirmationCode.Text)
            {


                using (SqlConnection con = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;AttachDbFilename=D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\Database\UnifiAppDatabase.mdf;Initial Catalog=UnifiAppDatabase;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"))
                {

                    con.Open();
                    ForgotlblUsername.Content = DataContainer.username;
                    if (ForgotNewUserPassword.Password == ForgotConfirmNewUserPassword.Password)
                    {
                        string password = ForgotConfirmNewUserPassword.Password;
                        string mySalt = BCrypt.Net.BCrypt.GenerateSalt(6);
                        string myHash = BCrypt.Net.BCrypt.HashPassword(password, mySalt);
                        SqlCommand sqlCommand = new SqlCommand("update dbo.Users SET Password='" + myHash + "'WHERE (Username='" + ForgotlblUsername.Content + "')", con);
                        sqlCommand.ExecuteNonQuery();
                        MessageBox.Show("Password has been changed!");
                        NavigationService.Navigate(new Uri("DashBoard.xaml", UriKind.Relative));
                    }
                    else
                    {
                        MessageBox.Show("Passwords don´t match!");
                    }
                    con.Close();

                }


            }
        }



    }
}
//}





