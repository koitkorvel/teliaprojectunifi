﻿#pragma checksum "..\..\SiteWlanSetup.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7490D2DDC455B417C9F5477FC84A3EEDA65757CE"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using UnifiApp;


namespace UnifiApp {
    
    
    /// <summary>
    /// SiteWlanSetup
    /// </summary>
    public partial class SiteWlanSetup : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SSIDTextbox;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox SSIDEnableCheckbox;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton SSIDOpenSecurity;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton SSIDWpaPersonalSecurity;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox CheckedHideSSID;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox GuestCheckedBox;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox SSIDPasswordTextbox;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SSIDPasswordTextblock;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SiteNameTextBlock;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox JsonSitesWlanSetup;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ExistingSitesWlanSetupCombobox;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ChangeExistingSiteWlanButton;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SiteIdTextblock;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SiteUserGroupsTextBlock;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox SiteUserGroupsComboBox;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox JsonSiteUserGroupsTextBox;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AddNewUserGroup;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SiteWlanGroupsTextBlock;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox SiteWlanGroupsComboBox;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\SiteWlanSetup.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox JsonSitesWlanGroupsTextBox;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/UnifiApp;component/sitewlansetup.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\SiteWlanSetup.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.SSIDTextbox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.SSIDEnableCheckbox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 3:
            this.SSIDOpenSecurity = ((System.Windows.Controls.RadioButton)(target));
            
            #line 19 "..\..\SiteWlanSetup.xaml"
            this.SSIDOpenSecurity.Checked += new System.Windows.RoutedEventHandler(this.SSIDOpenSecurity_Checked);
            
            #line default
            #line hidden
            return;
            case 4:
            this.SSIDWpaPersonalSecurity = ((System.Windows.Controls.RadioButton)(target));
            
            #line 21 "..\..\SiteWlanSetup.xaml"
            this.SSIDWpaPersonalSecurity.Checked += new System.Windows.RoutedEventHandler(this.SSIDWpaPersonalSecurity_Checked);
            
            #line default
            #line hidden
            return;
            case 5:
            this.CheckedHideSSID = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 6:
            this.GuestCheckedBox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 7:
            
            #line 45 "..\..\SiteWlanSetup.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.SubmitWlanSetupButton_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.SSIDPasswordTextbox = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 9:
            this.SSIDPasswordTextblock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.SiteNameTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.JsonSitesWlanSetup = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.ExistingSitesWlanSetupCombobox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 13:
            this.ChangeExistingSiteWlanButton = ((System.Windows.Controls.Button)(target));
            
            #line 71 "..\..\SiteWlanSetup.xaml"
            this.ChangeExistingSiteWlanButton.Click += new System.Windows.RoutedEventHandler(this.ChangeExistingSiteWlanButton_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.SiteIdTextblock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            this.SiteUserGroupsTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 16:
            this.SiteUserGroupsComboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 17:
            this.JsonSiteUserGroupsTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.AddNewUserGroup = ((System.Windows.Controls.Button)(target));
            
            #line 91 "..\..\SiteWlanSetup.xaml"
            this.AddNewUserGroup.Click += new System.Windows.RoutedEventHandler(this.AddNewUserGroup_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.SiteWlanGroupsTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 20:
            this.SiteWlanGroupsComboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 21:
            this.JsonSitesWlanGroupsTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

