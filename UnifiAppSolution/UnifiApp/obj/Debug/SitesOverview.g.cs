﻿#pragma checksum "..\..\SitesOverview.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "904196A0EE29861E148515C166F9083969F4EFFD"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using UnifiApp;


namespace UnifiApp {
    
    
    /// <summary>
    /// SitesOverview
    /// </summary>
    public partial class SitesOverview : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 25 "..\..\SitesOverview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button GetSitesOverview;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\SitesOverview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox SitesOverviewListbox;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\SitesOverview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SitesOverviewJsonTextBox;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\SitesOverview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SitesList;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\SitesOverview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SitesListTextBox;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\SitesOverview.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ExportCsvOverviewButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/UnifiApp;component/sitesoverview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\SitesOverview.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.GetSitesOverview = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\SitesOverview.xaml"
            this.GetSitesOverview.Click += new System.Windows.RoutedEventHandler(this.GetSitesOverview_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.SitesOverviewListbox = ((System.Windows.Controls.ListBox)(target));
            return;
            case 3:
            this.SitesOverviewJsonTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.SitesList = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.SitesListTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.ExportCsvOverviewButton = ((System.Windows.Controls.Button)(target));
            
            #line 44 "..\..\SitesOverview.xaml"
            this.ExportCsvOverviewButton.Click += new System.Windows.RoutedEventHandler(this.ExportCsvOverviewButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

