﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnifiApp
{
    /// <summary>
    /// Interaction logic for AddNewUserGroup.xaml
    /// </summary>
    public partial class AddNewUserGroup : Page
    {
        static List<JsonUserGroups> userGroupList = new List<JsonUserGroups>();
        const String randomStringGenerator = "abcdefghijklmnopqrstuvwxyz0123456789";
        private static Random random = new Random((int)DateTime.Now.Ticks);

        public AddNewUserGroup()
        {
            InitializeComponent();
            JsonListSitesTextbox.Visibility = Visibility.Hidden;
            String allSites = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSites.json");
            JsonListSitesTextbox.Text = allSites;
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonListSitesTextbox.Text, typeof(List<JsonSites>));
            List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();

            foreach (JsonSites jsonSites in sortedSitesJsonList)
            {

                ListSitesUserGroupCombobox.Items.Add(jsonSites.desc);
                string SiteId = jsonSites.name;
                string Site_Id = jsonSites._id;
            }
        }
        private static string RandomStringGenerator(int length)
        {
            var randomBuilder = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                var c = randomStringGenerator[random.Next(0, randomStringGenerator.Length)];
                randomBuilder.Append(c);
            }
            return randomBuilder.ToString();
        }

        private void AddUserGroupButton_Click(object sender, RoutedEventArgs e)
        {
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonListSitesTextbox.Text, typeof(List<JsonSites>));
            List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();


            string selectedSite = ListSitesUserGroupCombobox.SelectedItem.ToString();

            foreach (JsonSites sites in sortedSitesJsonList)
            {
                if(selectedSite == sites.desc)
                {
                    var filePath = @"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonUserGroups.json";
                    // Read existing json data
                    userGroupList = JsonConvert.DeserializeObject<List<JsonUserGroups>>(System.IO.File.ReadAllText(filePath));




                    JsonUserGroups newJsonUserGroups = new JsonUserGroups
                    {
                        _id = RandomStringGenerator(25),
                        name = UserGroupNameTextBox.Text,
                        attr_hidden_id = "Default",
                        attr_no_delete = true,
                        qos_rate_max_down = int.Parse(UserGroupDownloadTextBox.Text),
                        qos_rate_max_up = int.Parse(UserGroupUploadTextBox.Text),
                        site_id = sites._id,

                    

                    };
                    userGroupList.Add(newJsonUserGroups);


                    // Update json data string
                    String json = JsonConvert.SerializeObject(userGroupList, Formatting.Indented);


                    System.IO.File.WriteAllText(filePath, json);
                    string correctJson = File.ReadAllText(filePath);
                    //MessageBox.Show(correctJson);
                    MessageBox.Show("Added");
                }
            }
            
        }
    }
}
