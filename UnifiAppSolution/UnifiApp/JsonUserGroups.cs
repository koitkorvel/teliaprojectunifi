﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnifiApp
{
    //    [
    //    {
    //        "_id": "59f71724f5721868d6ae95af",
    //        "attr_hidden_id": "Default",
    //        "attr_no_delete": true,
    //        "name": "Default",
    //        "qos_rate_max_down": -1,
    //        "qos_rate_max_up": -1,
    //        "site_id": "59f71724f5721868d6ae95a7"
    //    }
    //    ]
    class JsonUserGroups
    {

        public string _id { get; set; }
        public string attr_hidden_id { get; set; }
        public bool attr_no_delete { get; set; }
        public string name { get; set; }
        public int qos_rate_max_down { get; set; }
        public int qos_rate_max_up { get; set; }
        public string site_id { get; set; }
        }

    }

