﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnifiApp
{
    class JsonWlanGroups
    {
        public string _id { get; set; }
        public string attr_hidden_id { get; set; }
        public bool attr_no_delete { get; set; }
        public string name { get; set; }
        public string site_id { get; set; }
        public bool attr_hidden { get; set; }
        public bool attr_no_edit { get; set; }
    }

}
