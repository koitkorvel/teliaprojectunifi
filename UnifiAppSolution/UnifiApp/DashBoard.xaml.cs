﻿using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UnifiApp
{
    /// <summary>
    /// Interaction logic for DashBoard.xaml
    /// </summary>
    public partial class DashBoard : Page
    {
        public DashBoard()
        {
            InitializeComponent();
            ShowsNavigationUI = false;
            SitesHealthTextbox.Visibility = Visibility.Hidden;
            SitesHealthListbox.Visibility = Visibility.Hidden;
            SitesOverviewListBox.Visibility = Visibility.Hidden;
            SitesOverviewTextBox.Visibility = Visibility.Hidden;
            SitesListTextBox.Visibility = Visibility.Hidden;
            SitesList.Visibility = Visibility.Hidden;

        }

        private void deserializeSitesJson(string JSON)
        {
            var newline = Environment.NewLine;

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonSites>));
            List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();


            int sitesCounter = 0;



            foreach (JsonSites json in sortedSitesJsonList)
            {
                //comboboxi oma
                var allSitesInfo =
                    ("Id:  ") + json._id + "," + newline +
                    ("Description:  ") + json.desc + "," + newline +
                    ("Site name:  ") + json.name + "," + newline +
                    ("Role:  ") + json.role + "," + newline;

                sitesCounter++;
                SitesList.AppendText(allSitesInfo + newline);
            }
        }

        private void ReportsButton_Click(object sender, RoutedEventArgs e)
        {
        
            NavigationService.Navigate(new Uri("ReportsPage.xaml", UriKind.Relative));
        }

        private void GetAllSitesDetailsInformationButton_Click(object sender, RoutedEventArgs e)
        {
            

        }

        private void GoToSitesOverviewButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("SitesOverview.xaml", UriKind.Relative));

        }

        private void ChangePasswordButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("ChangeUserPassword.xaml", UriKind.Relative));
        }

        private void GoToSitesHealth_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("SitesHealth.xaml", UriKind.Relative));
        }

        private void CloseProgramButton_Click(object sender, RoutedEventArgs e)
        {

            System.Windows.Application.Current.Shutdown();

            System.Windows.Forms.Application.Restart();

        }
        

        private void AddNewSiteButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("AddNewSite.xaml", UriKind.Relative));
        }
        
        //komadega
        private String SiteColumnNames = ",SiteID:,SiteName:,Alerts:,SubSystem:,LanActive:,LanInactive:,LanPending:,SubSystem:,WlanActive:,WlanInactive:,WlanPending:,Users:,Guests:,Status:,";

        //komadega versioon
        private String SiteHealthData = ",{0:10},{1:10},{2:10},{3:20},{4,0},{5,0},{6,0},{7,0},{8,0},{9,0},{10,0},{11,2},{12,0},{13,0},";
        private int correctwlanCounter;
        private int correctActiveWlanCounter;

        private void SitesHealthButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void deserializeSitesHealthJson(string JSON)
        {
            SitesHealthListbox.Items.Add(SiteColumnNames);
            var newline = Environment.NewLine;

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSitesHealth> correctSitesHealthJsonList = (List<JsonSitesHealth>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonSitesHealth>));
            List<JsonSitesHealth> sortedSitesHealthJsonList = correctSitesHealthJsonList.OrderBy(o => o.desc).ToList();

            foreach (JsonSitesHealth json in sortedSitesHealthJsonList)
            {

                //JsonFailist tõmmatud 
                var allSitesHealthInfo =
                    ("Site id:  ") + json._id + "," + newline +
                    ("Site name:  ") + json.desc + "," + newline +
                    ("Alerts:  ") + json.num_new_alarms + "," + newline +
                    ("Subsystem:  ") + json.healthLan.subsystem + "," + newline +
                    ("Lan Active:  ") + json.healthLan.num_ap + "," + newline +
                    ("Lan Inactive:  ") + json.healthLan.num_disconnected + "," + newline +
                    ("Lan Pending:  ") + json.healthLan.num_pending + "," + newline +
                    ("Subsystem:  ") + json.healthWlan.subsystem + "," + newline +
                    ("Wlan Active:  ") + json.healthWlan.num_ap + "," + newline +
                    ("Wlan Inactive:  ") + json.healthWlan.num_disconnected + "," + newline +
                    ("Wlan Pending:  ") + json.healthWlan.num_pending + "," + newline +
                    ("Users:  ") + json.healthWlan.num_user + "," + newline +
                    ("Guests:  ") + json.healthWlan.num_guest + "," + newline +
                    ("Status:  ") + json.healthWlan.status + "," + newline;

                //seob ära
                String SiteId, SiteName, Alerts, SubSystem, LanActive, LanInActive, LanPending, Subsystem, WlanActive, WlanInActive, WlanPending, Users, Guests, Status;

                //Json failist tõmmatud
                SiteId = json._id + (" ");
                SiteName = json.desc + (" ");
                Alerts = json.num_new_alarms.ToString() + (" ");
                SubSystem = json.healthLan.subsystem + (" ");
                LanActive = json.healthLan.num_ap.ToString() + (" ");
                LanInActive = json.healthLan.num_disconnected.ToString() + (" ");
                LanPending = json.healthLan.num_pending.ToString() + (" ");
                Subsystem = json.healthWlan.subsystem + (" ");
                WlanActive = json.healthWlan.num_ap.ToString() + (" ");
                WlanInActive = json.healthWlan.num_disconnected.ToString() + (" ");
                WlanPending = json.healthWlan.num_pending.ToString() + (" ");
                Users = json.healthWlan.num_user.ToString() + (" ");
                Guests = json.healthWlan.num_guest.ToString() + (" ");
                Status = json.healthWlan.status + (" ");

                //lisab kindla formatiga
                SitesHealthListbox.Items.Add(String.Format(SiteHealthData, SiteId, SiteName, Alerts, SubSystem, LanActive, LanInActive, LanPending, Subsystem, WlanActive, WlanInActive, WlanPending, Users, Guests, Status));

            }
        }

        private void ExportToCsvSiteHealth_Click(object sender, RoutedEventArgs e)
        {
            SitesHealthListbox.Items.Clear();
            //var webClient = new WebClient();
            //string sitesHealthJson = webClient.DownloadString("http://localhost:82/UNIFI/index.php?action=stat_sites&output_format=json");
            String sitesHealthJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSitesHealth.json");
            SitesHealthTextbox.Text = sitesHealthJson;
            deserializeSitesHealthJson(SitesHealthTextbox.Text);

            if (SitesHealthListbox.Items.Count > 0)
            {

                using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "CSV|*.csv", ValidateNames = true })
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        using (StreamWriter file = new StreamWriter(new FileStream(sfd.FileName, FileMode.Create), Encoding.UTF8))
                        {
                            StringBuilder csv = new StringBuilder();

                            foreach (string item in SitesHealthListbox.Items)
                            {
                                string correctItem = item.Replace("  ", ",");
                                file.WriteLine(correctItem);
                            }
                            file.Close();
                        }
                        System.Windows.MessageBox.Show("All data has been exported to CSV file!");
                    }
            }
            else
            {
                System.Windows.MessageBox.Show("Please select sites health data!");
            }
        }

        private void MoveDeviceButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("MoveDevice.xaml", UriKind.Relative));
        }

        private void ExportToCsvSiteHealth_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            
        }



        private void AddNewSiteButton_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            //AddNewSiteButton.Foreground = SystemColors.ActiveCaptionTextBrush;
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("SummerTest.xaml", UriKind.Relative));
        }

        //private void AddNewSiteButton_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        //{
        //    //AddNewSiteButton.Foreground = SystemColors.GrayTextColor
        //}


    }
}

