﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnifiApp
{
    /// <summary>
    /// Interaction logic for UnifiWebBrowser.xaml
    /// </summary>
    public partial class UnifiWebBrowser : Page
    {
        public UnifiWebBrowser()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            UnifiWeb.Source= new Uri("http://localhost:82/UNIFI");
        }

        private void UnifiWebBackBtn_Click(object sender, RoutedEventArgs e)
        {
            UnifiWeb.GoBack();
        }

        private void UnifiWebForwardBtn_Click(object sender, RoutedEventArgs e)
        {

            UnifiWeb.GoForward();
        }
    }
}
