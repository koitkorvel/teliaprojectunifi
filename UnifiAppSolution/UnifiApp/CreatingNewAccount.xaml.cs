﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Net.Mail;
using DevOne.Security.Cryptography.BCrypt;


namespace UnifiApp
{
    /// <summary>
    /// Interaction logic for CreatingNewAccount.xaml
    /// </summary>
    public partial class CreatingNewAccount : Page
    {
        string connectionString = @"Data Source=(localdb)\MSSQLLocalDB;AttachDbFilename=D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\Database\UnifiAppDatabase.mdf;Initial Catalog = UnifiAppDatabase; Integrated Security = True; Connect Timeout = 30; Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;";
        public CreatingNewAccount()
        {
            InitializeComponent();
            ShowsNavigationUI = true;
            CreateNewAccountBtn.Visibility = Visibility.Hidden;
            CreateButtonBorder.Visibility = Visibility.Hidden;
            ConfirmationCodeTextbox.Visibility = Visibility.Hidden;
            TestConfirmationCode.Visibility = Visibility.Hidden;


        }

        private void CreateNewAccountBtn_Click(object sender, RoutedEventArgs e)
        {

            if (creatingUsernameTxtbox.Text == "" || creatingNameTxtbox.Text == "" || creatingEmailTxtbox.Text == "" || creatingPasswordbox.Password == null)
                MessageBox.Show("Please fill all fields!");
            else if (creatingPasswordbox.Password != creatingPasswordboxConfirm.Password)
                MessageBox.Show("Password don´t match!");

            else
            {

                using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();
                    bool userExists = false;
                    using (SqlCommand cmd = new SqlCommand("select count(*) from dbo.Users where Username = @Username", sqlConnection))
                    {
                        cmd.Parameters.AddWithValue("Username", creatingUsernameTxtbox.Text);
                        userExists = (int)cmd.ExecuteScalar() > 0;
                    }
                    if (userExists)
                    {
                        MessageBox.Show("User already exists!");
                    }
                    //else if (creatingEmailTxtbox.Text.Contains("@telia.ee"))
                    else if (creatingEmailTxtbox.Text.Contains("@gmail.com"))
                    {
                        SqlCommand command = new SqlCommand(@"INSERT INTO dbo.Users(Username, Name, Email, Password)
                            VALUES(@Username, @Name, @Email, @Password)");
                        command.Connection = sqlConnection;
                        command.Parameters.AddWithValue("@Username", creatingUsernameTxtbox.Text.Trim());
                        command.Parameters.AddWithValue("@Name", creatingNameTxtbox.Text.Trim());
                        command.Parameters.AddWithValue("@Email", creatingEmailTxtbox.Text.Trim());
                        string password = creatingPasswordboxConfirm.Password;


                        string mySalt = BCrypt.Net.BCrypt.GenerateSalt(6);
                        string myHash = BCrypt.Net.BCrypt.HashPassword(password, mySalt);
                        command.Parameters.AddWithValue("@Password", myHash);
                        command.ExecuteNonQuery();
                        MessageBox.Show("Account registration complete!");
                        NavigationService.Navigate(new Uri("LoginPage.xaml", UriKind.Relative));
                    }

                    else
                    {
                        MessageBox.Show("Email must be @telia.ee");


                    }
                    sqlConnection.Close();
                }
            }

        }


        private void CancelAccountRegisteringButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("LoginPage.xaml", UriKind.Relative));
        }

        private void SendConfirmationCodeToEmailButton_Click(object sender, RoutedEventArgs e)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                bool userEmailExists = false;
                using (SqlCommand cmd = new SqlCommand("select count(*) from dbo.Users where Email = @Email", sqlConnection))
                {
                    cmd.Parameters.AddWithValue("Email", creatingEmailTxtbox.Text);
                    userEmailExists = (int)cmd.ExecuteScalar() > 0;
                }
                bool userExists = false;
                using (SqlCommand cmd = new SqlCommand("select count(*) from dbo.Users where Username = @Username", sqlConnection))
                {
                    cmd.Parameters.AddWithValue("Username", creatingUsernameTxtbox.Text);
                    userExists = (int)cmd.ExecuteScalar() > 0;
                }

                if (userExists)
                {
                    MessageBox.Show("User with this username already exists!");
                }
                else if (userEmailExists)
                {
                    MessageBox.Show("User with this email already exists!");
                }

                //else if (creatingEmailTxtbox.Text.Contains("@telia.ee"))
                else if (creatingEmailTxtbox.Text.Contains("@gmail.com"))
                {
                    Guid guidvalue = Guid.NewGuid();
                    MD5 mD5 = MD5.Create();
                    Guid hashed = new Guid(mD5.ComputeHash(guidvalue.ToByteArray()));
                    TestConfirmationCode.AppendText(hashed.ToString());

                    SmtpClient smtpClient = new SmtpClient();
                    smtpClient.Port = 587;
                    smtpClient.Host = "smtp.gmail.com";
                    smtpClient.EnableSsl = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new System.Net.NetworkCredential("unifitestingapp@gmail.com", "Triikraud123");

                    System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();

                    mailMessage.From = new MailAddress("unifitestingapp@gmail.com");
                    mailMessage.To.Add(creatingEmailTxtbox.Text);

                    mailMessage.Subject = "Unifi App account registration!";
                    mailMessage.Body = "Confirmation code is: " + hashed;

                    MessageBox.Show("Confirmation code is sent to: " + creatingEmailTxtbox.Text);
                    smtpClient.Send(mailMessage);
                    ConfirmationCodeTextbox.Visibility = Visibility.Visible;
                    if (ConfirmationCodeTextbox != null)
                    {
                        CreateNewAccountBtn.Visibility = Visibility.Visible;
                        CreateButtonBorder.Visibility = Visibility.Visible;
                        SendConfirmationCodeToEmailButton.Visibility = Visibility.Hidden;
                        SendCodeButtonBorder.Visibility = Visibility.Hidden;
                    }
                }
                else
                {

                    MessageBox.Show("Email must be @telia.ee");
                }

            }
        }
    }
}
