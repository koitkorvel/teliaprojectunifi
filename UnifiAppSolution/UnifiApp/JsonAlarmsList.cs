﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



//{
//        "_id": "5a76d2ebe082312dcefe43c3",
//        "ap": "f0:9f:c2:f6:d3:bf",
//        "ap_name": "f0:9f:c2:f6:d3:bf",
//        "archived": false,
//        "datetime": "2018-02-04T09:31:23Z",
//        "key": "EVT_AP_Lost_Contact",
//        "msg": "AP[f0:9f:c2:f6:d3:bf] was disconnected",
//        "site_id": "5a4ccc5e36e81d977b64b6be",
//        "subsystem": "wlan",
//        "time": 1517736683577
//    }
namespace UnifiApp
{
    class JsonAlarmsList
    {
        public string _id { get; set; }
        public string ap { get; set; }
        public string ap_name { get; set; }
        public bool archived { get; set; }
        public DateTime datetime { get; set; }
        public string key { get; set; }
        public string msg { get; set; }
        public string site_id { get; set; }
        public string subsystem { get; set; }
        //public long time { get; set; }

        public string time { get; set; }
    }

    }

