﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevOne.Security.Cryptography.BCrypt;

namespace UnifiApp
{
    /// <summary>
    /// Interaction logic for ChangeUserPassword.xaml
    /// </summary>
    public partial class ChangeUserPassword : Page
    {


        public ChangeUserPassword()
        {
            InitializeComponent();
            //lblUsername.Visibility = Visibility.Hidden;
            ShowsNavigationUI = true;
            lblUsername.Content = DataContainer.username;



        }

        private void ChangeUserPasswordButton_Click(object sender, RoutedEventArgs e)
        {

            using (SqlConnection con = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;AttachDbFilename=D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\Database\UnifiAppDatabase.mdf;Initial Catalog=UnifiAppDatabase;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT Username, Password FROM dbo.Users WHERE Username = @Username", con))
                {
                    string username = DataContainer.username;
                    string oldpassword = OldUserPassword.Password;
                    string oldpasswordHash = string.Empty;
                    cmd.Parameters.AddWithValue("@Username", username.ToString());
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.Read())
                        {
                            oldpasswordHash = sdr["Password"].ToString();
                            lblUsername.Content = username;

                        }
                    }
                    if (oldpassword == "")
                    {
                        MessageBox.Show("Invalid old password!");
                    }
                    else
                    {
                        if (NewUserPassword.Password == ConfirmNewUserPassword.Password)
                        {
                            bool correct = BCrypt.Net.BCrypt.Verify(oldpassword, oldpasswordHash);
                            if (correct)
                            {

                                string password = ConfirmNewUserPassword.Password;

                                string newSalt = BCryptHelper.GenerateSalt(6);
                                var newHash = BCryptHelper.HashPassword(password, newSalt);
                                SqlCommand sqlCommand = new SqlCommand("update dbo.Users SET Password='" + newHash.ToString() + "'WHERE (Username='" + lblUsername.Content + "')", con);

                                sqlCommand.ExecuteNonQuery();
                                MessageBox.Show("Password has been changed!");
                                NavigationService.Navigate(new Uri("DashBoard.xaml", UriKind.Relative));
                            }
                            con.Close();
                        }
                        con.Close();
                    }
                }

            }
        }
    }
}
