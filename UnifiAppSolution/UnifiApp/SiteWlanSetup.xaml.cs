﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnifiApp
{
    /// <summary>
    /// Interaction logic for SiteWlanSetup.xaml
    /// </summary>
    public partial class SiteWlanSetup : Page
    {
        private string usergroupID = string.Empty;
        private string wlangroupID = string.Empty;
        private string correctsecurity = string.Empty;
        private string correctguest = string.Empty;
        AddNewSite newSite = new AddNewSite();
        public SiteWlanSetup()
        {
            InitializeComponent();
            JsonSitesWlanGroupsTextBox.Visibility = Visibility.Hidden;
            JsonSiteUserGroupsTextBox.Visibility = Visibility.Hidden;
            JsonSitesWlanSetup.Visibility = Visibility.Hidden;
            SiteNameTextBlock.Text = DataContainer.siteName;
            ShowsNavigationUI = true;
            SSIDPasswordTextblock.Visibility = Visibility.Hidden;
            SSIDPasswordTextbox.Visibility = Visibility.Hidden;

            String allSites = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSites.json");
            JsonSitesWlanSetup.Text = allSites;

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonSitesWlanSetup.Text, typeof(List<JsonSites>));
            List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();

            foreach (JsonSites newSitejson in correctSitesJsonList)
            {

                if (newSitejson.desc == SiteNameTextBlock.Text)
                {
                    string siteId = newSitejson.name;
                    SiteIdTextblock.Text = siteId;
                }
            }
            foreach (JsonSites existingSiteJson in sortedSitesJsonList)
            {
                ExistingSitesWlanSetupCombobox.Items.Add(existingSiteJson.desc);
            }

        }
        private void SSIDWpaPersonalSecurity_Checked(object sender, RoutedEventArgs e)
        {

            if (SSIDWpaPersonalSecurity.IsChecked == true)
            {
                SSIDPasswordTextblock.Visibility = Visibility.Visible;
                SSIDPasswordTextbox.Visibility = Visibility.Visible;
            }
        }

        private void SSIDOpenSecurity_Checked(object sender, RoutedEventArgs e)
        {
            SSIDPasswordTextblock.Visibility = Visibility.Hidden;
            SSIDPasswordTextbox.Visibility = Visibility.Hidden;
        }

        private void SSIDWpaEnterprise_Checked(object sender, RoutedEventArgs e)
        {
            SSIDPasswordTextblock.Visibility = Visibility.Hidden;
            SSIDPasswordTextbox.Visibility = Visibility.Hidden;
        }

        private void ChangeExistingSiteWlanButton_Click(object sender, RoutedEventArgs e)
        {
            SiteUserGroupsComboBox.Items.Clear();
            SiteWlanGroupsComboBox.Items.Clear();
            //jsoni saamine
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonSitesWlanSetup.Text, typeof(List<JsonSites>));
            List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();
            ExistingSitesWlanSetupCombobox.Visibility = Visibility.Visible;

            //valitud site
            string selectedSite = ExistingSitesWlanSetupCombobox.SelectedItem.ToString();
            if (ExistingSitesWlanSetupCombobox.SelectedItem == null)
            {
                MessageBox.Show("Please select site!");
            }

            //käib läbi site
            foreach (JsonSites existingSite in sortedSitesJsonList)
            {

                if (selectedSite == existingSite.desc)
                {
                    string SiteId = existingSite._id;
                    string SiteName = existingSite.desc;

                    string filePathWlanGroups = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonWlanGroups.json");
                    string filePathUserGroups = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonUserGroups.json");
                    JsonSitesWlanGroupsTextBox.Text = filePathWlanGroups;
                    JsonSiteUserGroupsTextBox.Text = filePathUserGroups;

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    List<JsonUserGroups> correctSitesJsonUserGroupsList = (List<JsonUserGroups>)serializer.Deserialize(JsonSiteUserGroupsTextBox.Text, typeof(List<JsonUserGroups>));

                    List<JsonWlanGroups> correctSitesJsonWlanGroupsList = (List<JsonWlanGroups>)serializer.Deserialize(JsonSitesWlanGroupsTextBox.Text, typeof(List<JsonWlanGroups>));

                    foreach (JsonUserGroups usergroup in correctSitesJsonUserGroupsList)
                    {

                        if (existingSite._id == usergroup.site_id)
                        {

                            SiteUserGroupsComboBox.Items.Add(usergroup.name);
                        }



                    }
                    foreach (var wlangroup in correctSitesJsonWlanGroupsList)
                    {
                        if (existingSite._id == wlangroup.site_id)
                        {

                            SiteWlanGroupsComboBox.Items.Add(wlangroup.name);
                        }
                    }

                    string selectedSiteId = existingSite.name;

                    SiteNameTextBlock.Text = selectedSite;
                }

            }

        }

        private void SubmitWlanSetupButton_Click(object sender, RoutedEventArgs e)
        {

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonSitesWlanSetup.Text, typeof(List<JsonSites>));
            List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();
            List<JsonUserGroups> correctSitesJsonUserGroupsList = (List<JsonUserGroups>)javaScriptSerializer.Deserialize(JsonSiteUserGroupsTextBox.Text, typeof(List<JsonUserGroups>));
            List<JsonWlanGroups> correctSitesJsonWlanGroupsList = (List<JsonWlanGroups>)javaScriptSerializer.Deserialize(JsonSitesWlanGroupsTextBox.Text, typeof(List<JsonWlanGroups>));
            List<JsonSiteWlanSSID> sitesWlanSSIDList = new List<JsonSiteWlanSSID>();


            var filePathSitesWlanSSIDList = @"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSitesWlanSetup.json";
            sitesWlanSSIDList = JsonConvert.DeserializeObject<List<JsonSiteWlanSSID>>(System.IO.File.ReadAllText(filePathSitesWlanSSIDList));
            string selectedSite = ExistingSitesWlanSetupCombobox.SelectedItem.ToString();

            foreach (JsonSites existingSite in sortedSitesJsonList)
            {

                if (selectedSite == existingSite.desc)
                {
                    string SiteId = existingSite.name;
                    string SiteName = existingSite.desc;
                    foreach (JsonUserGroups usergroup in correctSitesJsonUserGroupsList)
                    {
                        string correctUserGroup = SiteUserGroupsComboBox.SelectedItem.ToString();
                        if (correctUserGroup == usergroup.name)
                        {

                            usergroupID = usergroup._id;

                        }
                    }

                    foreach (var wlangroup in correctSitesJsonWlanGroupsList)
                    {
                        string correctWlanGroup = SiteWlanGroupsComboBox.SelectedItem.ToString();
                        if (correctWlanGroup == wlangroup.name)
                        {
                            wlangroupID = wlangroup._id;
                        }
                    }

                    string SiteSSID = SSIDTextbox.Text;
                    string SitePassword = SSIDPasswordTextbox.Password;

                    string selectedSiteId = existingSite.name;
                    SiteNameTextBlock.Text = selectedSite;
                    string security = SSIDOpenSecurity.IsChecked.ToString();
                    if (SSIDOpenSecurity.IsChecked == true)
                    {
                        correctsecurity = "open";
                        SitePassword = "madebykoit";
                    }
                    else
                    {

                        correctsecurity = "wpapsk";
                    }

                    JsonSiteWlanSSID jsonSiteWlanSSID = new JsonSiteWlanSSID()
                    {
                        name = SiteSSID,
                        x_passphrase = SitePassword,
                        usergroup_id = usergroupID,
                        wlangroup_id = wlangroupID,
                        enabled = bool.Parse(SSIDEnableCheckbox.IsChecked.ToString()),
                        hide_ssid = bool.Parse(CheckedHideSSID.IsChecked.ToString()),
                        is_guest = bool.Parse(GuestCheckedBox.IsChecked.ToString()),
                        security = correctsecurity,
                        site_id = SiteId

                    };

                    if (SitePassword.Length >= 8 && SitePassword.Length <= 63)
                    {
                        sitesWlanSSIDList.Add(jsonSiteWlanSSID);
                        String jsonSitesWlanSSID = JsonConvert.SerializeObject(sitesWlanSSIDList, Formatting.Indented);
                        File.WriteAllText(filePathSitesWlanSSIDList, jsonSitesWlanSSID);
                        MessageBox.Show("Added");
                    }
                    else
                    {
                        MessageBox.Show("Password minimum length is 8 characters and maximum length is 63 characters!");
                    }

                }

            }
        }

        private String convertToBoolString(bool? value)
        {
            return value == true ? "true" : "false";
        }



        private void AddNewUserGroup_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("AddNewUserGroup.xaml", UriKind.Relative));
        }


    }
}
