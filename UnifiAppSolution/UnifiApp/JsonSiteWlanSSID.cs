﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnifiApp
{
    public class JsonSiteWlanSSID
    {
        public string name { get; set; }
        public string x_passphrase { get; set; }
        //int to string
        public string usergroup_id { get; set; }
        //int to string
        public string wlangroup_id { get; set; }
        public bool enabled { get; set; }
        public bool hide_ssid { get; set; }
        public bool is_guest { get; set; }
        public string security { get; set; }
        public string site_id { get; set; }
    }
}
