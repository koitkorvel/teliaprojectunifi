﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace UnifiApp
{
    /// <summary>
    /// Interaction logic for ReportsPage.xaml
    /// </summary>
    public partial class ReportsPage : Page
    {
        public ReportsPage()
        {
            InitializeComponent();
            showWebsites();
            ShowsNavigationUI = true;
            FindSelectedSiteButton.Visibility = Visibility.Hidden;
            JsonSitesTextBox.Visibility = Visibility.Hidden;
            JsonTextBox.Visibility = Visibility.Hidden;
            HideAllListboxes();


            SelectedExportCSVListBox.Items.Add("Export all items");
            SelectedExportCSVListBox.Items.Add("Export matched time results");
            SelectedExportCSVListBox.Items.Add("Export filter results");

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void ReportFilterResults_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void ParseJsonBtn_Click(object sender, RoutedEventArgs e)
        {

        }
        public void deserializeSitesJson(string JSON)
        {

            //textboxi kaudu jsoni saamine
            var newline = Environment.NewLine;

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonSites>));
            //sorteeri description tähestikulises järjekorras
            List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();
            int sitesCounter = 0;


            // käib läbi sorteeritud listi
            foreach (JsonSites json in sortedSitesJsonList)
            {
                //comboboxi oma
                var allSitesInfo =
                    ("Id:  ") + json._id + "," + newline +
                    ("Description:  ") + json.desc + "," + newline +
                    ("Site name:  ") + json.name + "," + newline +
                    ("Role:  ") + json.role + "," + newline;

                sitesCounter++;


                ////listboxi lisamine
                //comboboxi lisamine

                AllSitesCombobox.Items.Add(
                                        sitesCounter + (". ") + ("Description: ") +
                                        json.desc + "," + newline +
                                        ("Name    ") + json.name + (",") + newline +
                                        ("Id: ") + json._id);
                

            }
        }

        //formaat
        private String SiteAlarmsColumnNames = ",Alarm id:,Access point:,Access point name:,Archived:,Datetime:,Key:,Message:,Site id:,Site name:,Subsystem type:,Time:,";
        //formaat
        private String SiteAlarmsData = ",{0:0},{1:0},{2:0},{3:0},{4,0},{5,0},{6,0},{7,0},{8,0},{9,0},{10,0},";
        public void deserializeAlarmsJSON(string JSON)
        {


            int counter = 0;
            //textboxi kaudu jsoni saamine
            var newline = Environment.NewLine;

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctJsonSitesList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonSitesTextBox.Text, typeof(List<JsonSites>));
            List<JsonAlarmsList> correctJsonList = (List<JsonAlarmsList>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonAlarmsList>));
            List<JsonAlarmsList> sortedAlarmsJsonList = correctJsonList.OrderBy(o => o.datetime).ToList();


            foreach (JsonSites item in correctJsonSitesList)
            {
                if (DataContainer.selectedSiteIdAlarms == item._id)
                {
                    DataContainer.siteName = item.desc;
                    //sorted by datetime
                    foreach (JsonAlarmsList json in sortedAlarmsJsonList)
                    {


                        if (json.site_id == DataContainer.selectedSiteIdAlarms)
                        {


                            var allAlarmsInfo =

                                ("Alarm id:  ") + json._id + "," + newline +
                                ("Access point:  ") + json.ap + "," + newline +
                                ("Access point name:  ") + json.ap_name + "," + newline +
                                ("Archived:  ") + json.archived + "," + newline +
                                ("Datetime:  ") + json.datetime + "," + newline +
                                ("Key:  ") + json.key + "," + newline +
                                ("Message:  ") + json.msg + "," + newline +
                                ("Site id:  ") + json.site_id + "," + newline +
                                ("Subsystem type:  ") + json.subsystem + "," + newline +
                                ("Time:  ") + json.time + "," + newline;


                            //listboxi oma

                            //formaat
                            String SiteName, AlarmId, AccessPoint, AccessPointName, Archived, DateTime, Key, Message, SiteId, SubsystemType, Time;
                            AlarmId = json._id + (" ");
                            AccessPoint = json.ap + (" ");
                            AccessPointName = json.ap_name + (" ");
                            Archived = json.archived + (" ");
                            DateTime = json.datetime + (" ");
                            Key = json.key + (" ");
                            Message = json.msg + (" ");
                            SiteId = json.site_id + (" ");
                            SiteName = DataContainer.siteName + (" ");
                            SubsystemType = json.subsystem + (" ");
                            Time = json.time + (" ");

                            //lisab kindla formaadiga
                            ParsedJsonListBox.Items.Add(String.Format(SiteAlarmsData, AlarmId, AccessPoint, AccessPointName, Archived, DateTime, Key, Message, SiteId, SiteName, SubsystemType, Time));
                            counter++;
                        }

                        CountNumbertTxtBlock.Text = counter.ToString();
                        if (ParsedJsonListBox.Items.Count == 0)
                        {
                            CountNumbertTxtBlock.Text = "0";
                        }
                        ShowOnlyParsedListbox();



                    }
                }
            }
        }
        public void deserializeAlarmsDatesJSON(string JSON)
        {

            var newline = Environment.NewLine;
            string AllSiteNamesAlarms;
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctJsonSitesList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonSitesTextBox.Text, typeof(List<JsonSites>));
            List<JsonAlarmsList> correctJsonList = (List<JsonAlarmsList>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonAlarmsList>));
            List<JsonAlarmsList> sortedAlarmsJsonList = correctJsonList.OrderBy(o => o.datetime).ToList();


            foreach (JsonSites item in correctJsonSitesList)
            {

                if (DataContainer.selectedSiteIdAlarms == item._id)
                {

                    foreach (JsonAlarmsList json in sortedAlarmsJsonList)
                    {

                        if (json.site_id != item._id)
                        {
                            AllSiteNamesAlarms = null;
                        }
                        else if (json.site_id == item._id)
                        {

                            AllSiteNamesAlarms = item.desc;
                            if (json.datetime >= DataContainer.startTime && json.datetime <= DataContainer.endTime)
                            {
                                var allAlarmsInfo =

                                ("Alarm id:  ") + json._id + "," + newline +
                                ("Access point:  ") + json.ap + "," + newline +
                                ("Access point name:  ") + json.ap_name + "," + newline +
                                ("Archived:  ") + json.archived + "," + newline +
                                ("Datetime:  ") + json.datetime + "," + newline +
                                ("Key:  ") + json.key + "," + newline +
                                ("Message:  ") + json.msg + "," + newline +
                                ("Site id:  ") + json.site_id + "," + newline +
                                ("Subsystem type:  ") + json.subsystem + "," + newline +
                                ("Time:  ") + json.time + "," + newline;


                                //listboxi oma

                                //formaat
                                String SiteName, AlarmId, AccessPoint, AccessPointName, Archived, DateTime, Key, Message, SiteId, SubsystemType, Time;
                                AlarmId = json._id + (" ");
                                AccessPoint = json.ap + (" ");
                                AccessPointName = json.ap_name + (" ");
                                Archived = json.archived + (" ");
                                DateTime = json.datetime + (" ");
                                Key = json.key + (" ");
                                Message = json.msg + (" ");
                                SiteId = json.site_id + (" ");
                                SiteName = AllSiteNamesAlarms;
                                SubsystemType = json.subsystem + (" ");
                                Time = json.time + (" ");

                                //lisab kindla formaadiga
                                ParsedJsonListBox.Items.Add(String.Format(SiteAlarmsData, AlarmId, AccessPoint, AccessPointName, Archived, DateTime, Key, Message, SiteId, SiteName, SubsystemType, Time));
                            }


                            ShowOnlyParsedListbox();

                            ParsedJsonTextBlock.Visibility = Visibility.Visible;

                            CountNumbertTxtBlock.Visibility = Visibility.Visible;
                            CountNumbertTxtBlock.Text = ParsedJsonListBox.Items.Count.ToString();
                            if (ParsedJsonListBox.Items.Count == 0)
                            {
                                CountNumbertTxtBlock.Text = "0";
                            }



                        }
                    }
                }

            }

        }


        public void deserializeEventsDatesJSON(string JSON)
        {
            int counter = 0;
            var newline = Environment.NewLine;
            string AllSitesNameEvents;
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctJsonSitesList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonSitesTextBox.Text, typeof(List<JsonSites>));
            List<JsonEventsList> correctJsonList = (List<JsonEventsList>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonEventsList>));
            List<JsonEventsList> sortedEventsJsonList = correctJsonList.OrderBy(o => o.datetime).ToList();

            //sorted by DateTime
            foreach (JsonSites item in correctJsonSitesList)
            {
                foreach (JsonEventsList json in sortedEventsJsonList)
                {
                    if (DataContainer.selectedSiteIdAlarms == item._id)
                    {
                        if (json.site_id != item._id)
                        {
                            AllSitesNameEvents = null;
                        }
                        else if (json.site_id == item._id)
                        {

                            AllSitesNameEvents = item.desc;
                            if (json.datetime >= DataContainer.startTime && json.datetime <= DataContainer.endTime)
                            {
                                //listboxi oma
                                var allEventsInfo =

                                ("Event id:  ") + json._id + "," + newline +
                                ("Admin:  ") + json.admin + "," + newline +
                                ("Datetime:  ") + json.datetime + "," + newline +
                                ("Ip:  ") + json.ip + "," + newline +
                                ("Is admin:  ") + json.is_admin + "," + newline +
                                ("Key:  ") + json.key + "," + newline +
                                ("Message:  ") + json.msg + "," + newline +
                                ("Site id:  ") + json.site_id + "," + newline +
                                ("Subsystem type:  ") + json.subsystem + "," + newline +
                                ("Time:  ") + json.time + "," + newline;

                                String EventId, Admin, Datetime, Ip, IsAdmin, Key, Message, SiteId, SiteName, SubsystemType, Time;

                                EventId = json._id + (" ");
                                Admin = json.admin + (" ");
                                Datetime = json.datetime + (" ");
                                Ip = json.ip + (" ");
                                IsAdmin = json.is_admin + (" ");
                                Key = json.key + (" ");
                                Message = json.msg + (" ");
                                SiteId = json.site_id + (" ");
                                SiteName = AllSitesNameEvents;
                                SubsystemType = json.subsystem + (" ");
                                Time = json.time + (" ");

                                //lisab kindla formaadiga
                                ParsedJsonListBox.Items.Add(String.Format(SiteEventsData, EventId, Admin, Datetime, Ip, IsAdmin, Key, Message, SiteId, SiteName, SubsystemType, Time));
                                counter++;
                                //listboxi lisamine
                                ParsedJsonListBox.Visibility = Visibility.Visible;



                            }


                        }
                        //}
                        ParsedJsonTextBlock.Visibility = Visibility.Visible;
                        CountNumbertTxtBlock.Text = counter.ToString();
                        CountNumbertTxtBlock.Visibility = Visibility.Visible;
                        if (ParsedJsonListBox.Items.Count == 0)
                        {
                            CountNumbertTxtBlock.Text = "0";
                        }
                    }
                }
            }
        }




        public void deserializeAllEventsJSON(string JSON)
        {

            int counter = 0;
            //textboxi kaudu jsoni saamine
            var newline = Environment.NewLine;
            string AllSitesNameEvents;
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctJsonSitesList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonSitesTextBox.Text, typeof(List<JsonSites>));
            List<JsonEventsList> correctJsonList = (List<JsonEventsList>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonEventsList>));
            List<JsonEventsList> sortedEventsJsonList = correctJsonList.OrderBy(o => o.datetime).ToList();

            //sorted by DateTime
            foreach (JsonSites item in correctJsonSitesList)
            {

                foreach (JsonEventsList json in sortedEventsJsonList)
                {
                    if (json.site_id != item._id)
                    {
                        AllSitesNameEvents = null;
                    }
                    else if (json.site_id == item._id)
                    {

                        AllSitesNameEvents = item.desc;

                        //listboxi oma
                        var allEventsInfo =

                            ("Event id:  ") + json._id + "," + newline +
                            ("Admin:  ") + json.admin + "," + newline +
                            ("Datetime:  ") + json.datetime + "," + newline +
                            ("Ip:  ") + json.ip + "," + newline +
                            ("Is admin:  ") + json.is_admin + "," + newline +
                            ("Key:  ") + json.key + "," + newline +
                            ("Message:  ") + json.msg + "," + newline +
                            ("Site id:  ") + json.site_id + "," + newline +
                            ("Subsystem type:  ") + json.subsystem + "," + newline +
                            ("Time:  ") + json.time + "," + newline;

                        String EventId, Admin, Datetime, Ip, IsAdmin, Key, Message, SiteId, SiteName, SubsystemType, Time;

                        EventId = json._id + (" ");
                        Admin = json.admin + (" ");
                        Datetime = json.datetime + (" ");
                        Ip = json.ip + (" ");
                        IsAdmin = json.is_admin + (" ");
                        Key = json.key + (" ");
                        Message = json.msg + (" ");
                        SiteId = json.site_id + (" ");
                        SiteName = AllSitesNameEvents;
                        SubsystemType = json.subsystem + (" ");
                        Time = json.time + (" ");

                        //lisab kindla formaadiga
                        ParsedJsonListBox.Items.Add(String.Format(SiteEventsData, EventId, Admin, Datetime, Ip, IsAdmin, Key, Message, SiteId, SiteName, SubsystemType, Time));
                        counter++;
                        CountNumbertTxtBlock.Text = counter.ToString();
                        //listboxi lisamine
                        ParsedJsonListBox.Visibility = Visibility.Visible;



                    }


                    //}
                    //}
                    ParsedJsonTextBlock.Visibility = Visibility.Visible;
                    CountNumbertTxtBlock.Visibility = Visibility.Visible;
                    if (ParsedJsonListBox.Items.Count == 0)
                    {
                        CountNumbertTxtBlock.Text = "0";
                    }
                }
            }
        }
        //formaat
        private String SiteEventsColumnNames = ",Event id:,Admin:,Datetime:,Ip:,Is Admin:,Key:,Message:,Site id:, Site name:,Subsystem type:,Time:,";
        //formaat
        private String SiteEventsData = ",{0:0},{1:0},{2:0},{3:0},{4,0},{5,0},{6,0},{7,0},{8,0},{9,0},{10,0},";
        private static List<JsonSites> sitesListReports;

        public void deserializeEventsJSON(string JSON)
        {
            int counter = 0;

            //textboxi kaudu jsoni saamine
            var newline = Environment.NewLine;

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctJsonSitesList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonSitesTextBox.Text, typeof(List<JsonSites>));
            List<JsonEventsList> correctJsonList = (List<JsonEventsList>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonEventsList>));
            List<JsonEventsList> sortedEventsJsonList = correctJsonList.OrderBy(o => o.datetime).ToList();

            //sorted by DateTime
            foreach (JsonSites item in correctJsonSitesList)
            {
                if (DataContainer.selectedSiteIdAlarms == item._id)
                {
                    DataContainer.siteName = item.desc;

                    foreach (JsonEventsList json in sortedEventsJsonList)
                    {
                        if (json.site_id == DataContainer.selectedSiteIdAlarms)
                        {



                            //listboxi oma
                            var allEventsInfo =

                                ("Event id:  ") + json._id + "," + newline +
                                ("Admin:  ") + json.admin + "," + newline +
                                ("Datetime:  ") + json.datetime + "," + newline +
                                ("Ip:  ") + json.ip + "," + newline +
                                ("Is admin:  ") + json.is_admin + "," + newline +
                                ("Key:  ") + json.key + "," + newline +
                                ("Message:  ") + json.msg + "," + newline +
                                ("Site id:  ") + json.site_id + "," + newline +
                                ("Subsystem type:  ") + json.subsystem + "," + newline +
                                ("Time:  ") + json.time + "," + newline;

                            String EventId, Admin, Datetime, Ip, IsAdmin, Key, Message, SiteId, SiteName, SubsystemType, Time;

                            EventId = json._id + (" ");
                            Admin = json.admin + (" ");
                            Datetime = json.datetime + (" ");
                            Ip = json.ip + (" ");
                            IsAdmin = json.is_admin + (" ");
                            Key = json.key + (" ");
                            Message = json.msg + (" ");
                            SiteId = json.site_id + (" ");
                            SiteName = DataContainer.siteName;
                            SubsystemType = json.subsystem + (" ");
                            Time = json.time + (" ");

                            //lisab kindla formaadiga
                            ParsedJsonListBox.Items.Add(String.Format(SiteEventsData, EventId, Admin, Datetime, Ip, IsAdmin, Key, Message, SiteId, SiteName, SubsystemType, Time));
                            counter++;
                            //listboxi lisamine
                            ParsedJsonListBox.Visibility = Visibility.Visible;
                            CountNumbertTxtBlock.Text = counter.ToString();
                            if (ParsedJsonListBox.Items.Count == 0)
                            {
                                CountNumbertTxtBlock.Text = "0";
                            }



                        }
                    }
                }
            }
        }


        private void GetJsonFromWebBtn_Click(object sender, RoutedEventArgs e)
        {
            //url kaudu jsoni saamine
            //string url = @"http://localhost:82/UNIFI/index.php?action=list_alarms";
            //var json = new WebClient().DownloadString(url);
            ////MessageBox.Show(json);

        }

        private void GetDataBtn_Click(object sender, EventArgs e)
        {
            var webGet = new HtmlWeb();
            var doc = webGet.Load("http://localhost:82/UNIFI/index.php?action=list_alarms");
            //webGet.Load("http://localhost:82/UNIFI/index.php?reset_session=true");
            //var doc = webGet.Load("http://localhost:82/UNIFI/index.php?site_id=wiqoij6v&site_name=Ad%20Angels");

            HtmlNode node = doc.DocumentNode.SelectSingleNode("//pre[@id='pre_output']");
            JsonTextBox.Text = node.InnerHtml;




            //textifaili kirjutamine
            StreamWriter File = new StreamWriter("C:/Users/koitko/Desktop/test.txt");
            File.Write(node.InnerHtml);
            File.Close();
            deserializeAlarmsJSON(JsonTextBox.Text);

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            ButtonGeneral.Visibility = Visibility.Visible;
            //Encoding enc = new UTF8Encoding(true, true);
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.ShowDialog();


            string filename = openFile.FileName;

            filenametxtblock.Text = filename;


            //hrefi värk
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.Load(filename);
            foreach (HtmlNode href in doc.DocumentNode.SelectNodes("//a[@href]"))
            {
                string hrefValue = href.GetAttributeValue("href", string.Empty);
                if (hrefValue.Contains("site_name"))
                {
                    string correct = hrefValue.Substring(hrefValue.LastIndexOf('=') + 1);
                    //formatted 
                    TestListBox.Items.Add(correct);

                }


            }

            //Jsoni saamine pre kaudu
            HtmlNode node = doc.DocumentNode.SelectSingleNode("//pre[@id='pre_output']");
            JsonTextBox.Text = node.InnerHtml;
            if (node.InnerHtml.Contains("admin"))
            {
                deserializeEventsJSON(JsonTextBox.Text);
            }
            else
            {
                deserializeAlarmsJSON(JsonTextBox.Text);
            }
        }

        private void TextClearBtn_Click(object sender, RoutedEventArgs e)
        {
            AllSitesCombobox.SelectedItem = null;
            StartDateSelected.Text = "";
            EndDateSelected.Text = "";
            SelectEventsDatesBeforeDownloadButton.Content = "Select events with date!";
            SelectAlarmsDatesBeforeDownloadButton.Content = "Select alarms with date!";
            DownloadAllWebsitesAlarmsButton.Content = "Download all website alarms";
            DownloadAllWebsiteEventsButton.Content = "Download all website events";
            DownloadOneWebsiteAlarmsButton.Content = "Download one website alarms";
            DownloadOneWebsiteEventsButton.Content = "Download one website events";
            ClearSearchAndFilterResultsButton.Visibility = Visibility.Visible;
            ClearSearchAndFilterButtonBorder.Visibility = Visibility.Visible;
            ClearFilterResultsButton.Visibility = Visibility.Visible;
            ClearFilterResultsButtonBorder.Visibility = Visibility.Visible;
            ButtonGeneral.Visibility = Visibility.Visible;
            ClearSearchResultsButton.Visibility = Visibility.Visible;
            //input json
            JsonTextBox.Clear();
            HideAllListboxes();
            //counterid
            CountNumbertTxtBlock.Text = 0.ToString();
            CounterTestListboxResultsTxtblock.Text = 0.ToString();
            FilterResultsListbox.Items.Clear();
            CounterFilterResultsTxtblock.Text = 0.ToString();

            //parsed json listbox
            ParsedJsonListBox.Items.Clear();

            //show site names
            TestListBox.Items.Clear();


        }

        private void HideAllListboxes()
        {
            ParsedJsonTextBlock.Visibility = Visibility.Hidden;
            CountNumbertTxtBlock.Visibility = Visibility.Hidden;
            ParsedJsonListBox.Visibility = Visibility.Hidden;
            CounterTestListboxResultsTxtblock.Visibility = Visibility.Hidden;
            CountTestListBoxResultsTxtBlock.Visibility = Visibility.Hidden;
            TestListBox.Visibility = Visibility.Hidden;
            CountFilterResultsTxtBlock.Visibility = Visibility.Hidden;
            CounterFilterResultsTxtblock.Visibility = Visibility.Hidden;
            FilterResultsListbox.Visibility = Visibility.Hidden;
        }

        private void ExportCsvBtn_Click(object sender, RoutedEventArgs e)
        {
            ExportToCsv();
        }

        private void ExportToCsv()
        {
            if (SelectedExportCSVListBox.Text == "Export all items")
            {
                if (ParsedJsonListBox.Items.Count > 0)
                {
                    ButtonGeneral.Visibility = Visibility.Visible;

                    using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "CSV|*.csv", ValidateNames = true })
                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            using (StreamWriter file = new StreamWriter(new FileStream(sfd.FileName, FileMode.Create), Encoding.UTF8))
                            {
                                StringBuilder csv = new StringBuilder();
                                foreach (string item in ParsedJsonListBox.Items)
                                {
                                    string correctItem = item.Replace("  ", "|");
                                    file.WriteLine(correctItem);
                                }
                                file.Close();
                            }
                            System.Windows.MessageBox.Show("All data has been exported to CSV file!");
                        }
                }
                else
                {
                    System.Windows.MessageBox.Show("No filter results to export!");
                }
            }
            else if (SelectedExportCSVListBox.Text == "Export matched time results")
            {
                if (TestListBox.Items.Count > 0)
                {
                    using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "CSV|*.csv", ValidateNames = true })
                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            using (StreamWriter file = new StreamWriter(new FileStream(sfd.FileName, FileMode.Create), Encoding.UTF8))
                            {
                                StringBuilder csv = new StringBuilder();
                                foreach (string item in TestListBox.Items)
                                {
                                    string correctItem = item.Replace("  ", "|");
                                    file.WriteLine(correctItem);
                                }
                                file.Close();
                            }
                            System.Windows.MessageBox.Show("Matched time results has been exported to CSV file!");
                        }
                }
                else
                {
                    System.Windows.MessageBox.Show("No matched time results to export!");


                }
            }
            else if (SelectedExportCSVListBox.Text == "Export filter results")
            {
                if (FilterResultsListbox.Items.Count > 0)
                {
                    using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "CSV|*.csv", ValidateNames = true })
                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            using (StreamWriter file = new StreamWriter(new FileStream(sfd.FileName, FileMode.Create), Encoding.UTF8))
                            {
                                StringBuilder csv = new StringBuilder();
                                foreach (string item in FilterResultsListbox.Items)
                                {
                                    string correctItem = item.Replace("  ", "|");
                                    file.WriteLine(correctItem);
                                }
                                file.Close();
                            }
                            System.Windows.MessageBox.Show("Filter results has been exported to CSV file!");
                        }
                }
                else
                {

                    System.Windows.MessageBox.Show("No filter results to export!");

                }
            }
        }

        private void ParseAlarmsJsonButton_Click(object sender, RoutedEventArgs e)
        {

            ButtonGeneral.Visibility = Visibility.Hidden;
            WebClient webclient = new WebClient();
            //netist json
            string alarmsJson = webclient.DownloadString("http://localhost:82/UNIFI/index.php?site_id=ep6r3cp3&site_name=Ad%20Angels&action=list_alarms&output_format=json");
            //beautify
            JToken parsedAlarmsJson = JToken.Parse(alarmsJson);
            parsedAlarmsJson.ToString(Newtonsoft.Json.Formatting.Indented);
            JsonTextBox.Text = parsedAlarmsJson.ToString();
            deserializeAlarmsJSON(JsonTextBox.Text);


        }

        private void ParseEventsJsonButton_Click(object sender, RoutedEventArgs e)
        {

            ButtonGeneral.Visibility = Visibility.Visible;
            var webClient = new WebClient();
            //netist json
            string eventsJson = webClient.DownloadString("http://localhost:82/UNIFI/index.php?site_id=ep6r3cp3&site_name=Ad%20Angels&action=list_events&output_format=json");
            //beautify
            JToken parsedEventsJson = JToken.Parse(eventsJson);
            parsedEventsJson.ToString(Newtonsoft.Json.Formatting.Indented);
            JsonTextBox.Text = parsedEventsJson.ToString();
            JsonTextBox.Text = parsedEventsJson.ToString();
            deserializeEventsJSON(JsonTextBox.Text);


        }
        private void ButtonAdmin_Click(object sender, RoutedEventArgs e)
        {
            CounterFilterResultsTxtblock.Text = 0.ToString();
            ShowOnlyFilterListbox();
            ClearFilterResultsButton.Visibility = Visibility.Visible;
            ClearFilterResultsButtonBorder.Visibility = Visibility.Visible;
            ClearSearchResultsButton.Visibility = Visibility.Visible;
            ClearSearchButtonBorder.Visibility = Visibility.Visible;

            //listboxi tühjendust, eelnevatest andmetest
            if (FilterResultsListbox.Items.Count > 0)
            {
                FilterResultsListbox.Items.Clear();

            }
            else
            {
                CounterFilterResultsTxtblock.Text = 0.ToString();
            }



            if (TestListBox.Items.Count > 0)
            {


                foreach (string admin in TestListBox.Items)



                    if (admin.Contains("admin"))
                    {
                        FilterResultsListbox.Items.Add(admin);

                        if (FilterResultsListbox.Items.IsEmpty)
                        {
                            CounterFilterResultsTxtblock.Text = 0.ToString();
                        }
                        CounterFilterResultsTxtblock.Text = FilterResultsListbox.Items.Count.ToString();

                    }


            }
            else if (ParsedJsonListBox.Items.Count > 0)
            {
                if (DownloadOneWebsiteAlarmsButton.Content == "Add one website alarms")
                {

                    //FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                }

                if (DownloadOneWebsiteEventsButton.Content == "Add one website events")
                {
                    FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                }
                if (SelectAlarmsDatesBeforeDownloadButton.Content == "Downloaded alarms!")
                {
                    //FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                }
                if (SelectEventsDatesBeforeDownloadButton.Content == "Downloaded events!")
                {
                    FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                }
                if (DownloadAllWebsiteEventsButton.Content == "All events downloaded")
                {
                    FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                }


                foreach (string admin in ParsedJsonListBox.Items)



                    if (admin.Contains("admin"))
                    {

                        FilterResultsListbox.Items.Add(admin);

                        if (FilterResultsListbox.Items.IsEmpty)
                        {
                            CounterFilterResultsTxtblock.Text = 0.ToString();
                        }
                        CounterFilterResultsTxtblock.Text = FilterResultsListbox.Items.Count.ToString();

                    }


            }



        }

        private void ButtonWlan_Click(object sender, RoutedEventArgs e)
        {

            ShowOnlyFilterListbox();
            ClearFilterResultsButton.Visibility = Visibility.Visible;
            ClearFilterResultsButtonBorder.Visibility = Visibility.Visible;
            ClearSearchResultsButton.Visibility = Visibility.Visible;
            ClearSearchButtonBorder.Visibility = Visibility.Visible;
            CounterFilterResultsTxtblock.Text = 0.ToString();



            //listboxi tühjendust, eelnevatest andmetest
            if (FilterResultsListbox.Items.Count > 0)
            {
                FilterResultsListbox.Items.Clear();
            }
            else
            {
                CounterFilterResultsTxtblock.Text = 0.ToString();
            }
            if (TestListBox.Items.Count > 0)
            {

                foreach (string wlan in TestListBox.Items)



                    if (wlan.Contains("wlan"))
                    {

                        FilterResultsListbox.Items.Add(wlan);

                        if (FilterResultsListbox.Items.IsEmpty)
                        {
                            CounterFilterResultsTxtblock.Text = 0.ToString();
                        }
                        CounterFilterResultsTxtblock.Text = FilterResultsListbox.Items.Count.ToString();


                    }

            }
            else if (ParsedJsonListBox.Items.Count > 0)
            {

                if (DownloadOneWebsiteAlarmsButton.Content == "Add one website alarms")
                {

                    FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                }

                if (DownloadOneWebsiteEventsButton.Content == "Add one website events")
                {
                    FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                }
                if (SelectAlarmsDatesBeforeDownloadButton.Content == "Downloaded alarms!")
                {
                    FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                }
                if (SelectEventsDatesBeforeDownloadButton.Content == "Downloaded events!")
                {
                    FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                }
                if (DownloadAllWebsitesAlarmsButton.Content == "All alarms downloaded")
                {
                    FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                }
                if (DownloadAllWebsiteEventsButton.Content == "All events downloaded")
                {
                    FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                }


                foreach (string wlan in ParsedJsonListBox.Items)



                    if (wlan.Contains("wlan"))
                    {


                        FilterResultsListbox.Items.Add(wlan);
                        if (FilterResultsListbox.Items.IsEmpty)
                        {
                            CounterFilterResultsTxtblock.Text = 0.ToString();
                        }
                        CounterFilterResultsTxtblock.Text = FilterResultsListbox.Items.Count.ToString();

                    }


            }


        }


        private void ButtonErrors_Click(object sender, RoutedEventArgs e)
        {
            ShowOnlyFilterListbox();

            ClearFilterResultsButton.Visibility = Visibility.Visible;
            ClearFilterResultsButtonBorder.Visibility = Visibility.Visible;
            ClearSearchResultsButton.Visibility = Visibility.Visible;
            ClearSearchButtonBorder.Visibility = Visibility.Visible;
            CounterFilterResultsTxtblock.Text = 0.ToString();

            //listboxi tühjendust, eelnevatest andmetest
            if (FilterResultsListbox.Items.Count > 0)
            {
                FilterResultsListbox.Items.Clear();

            }

            else
            {
                CounterFilterResultsTxtblock.Text = 0.ToString();
            }

            if (TestListBox.Items.Count > 0)
            {

                foreach (string error in TestListBox.Items)



                    if (error.Contains("was disconnected"))
                    {
                        FilterResultsListbox.Items.Add(error);

                        if (FilterResultsListbox.Items.IsEmpty)
                        {
                            CounterFilterResultsTxtblock.Text = 0.ToString();
                        }
                        CounterFilterResultsTxtblock.Text = FilterResultsListbox.Items.Count.ToString();

                    }


            }
            else if (ParsedJsonListBox.Items.Count > 0)
            {
                if (DownloadOneWebsiteAlarmsButton.Content == "Add one website alarms")
                {

                    FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                }

                if (DownloadOneWebsiteEventsButton.Content == "Add one website events")
                {
                    //FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                }
                if (SelectAlarmsDatesBeforeDownloadButton.Content == "Downloaded alarms!")
                {
                    FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                }
                if (SelectEventsDatesBeforeDownloadButton.Content == "Downloaded events!")
                {
                    FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                }

                if (DownloadAllWebsitesAlarmsButton.Content == "All alarms downloaded")
                {
                    FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                }
                foreach (string error in ParsedJsonListBox.Items)
                {



                    if (error.Contains("was disconnected"))
                    {
                        FilterResultsListbox.Items.Add(error);
                        if (FilterResultsListbox.Items.IsEmpty)
                        {
                            CounterFilterResultsTxtblock.Text = 0.ToString();
                        }
                        CounterFilterResultsTxtblock.Text = FilterResultsListbox.Items.Count.ToString();


                    }

                }

            }
        }
        private void ShowOnlyTestlistbox()
        {
            CountTestListBoxResultsTxtBlock.Visibility = Visibility.Visible;
            CounterTestListboxResultsTxtblock.Visibility = Visibility.Visible;
            TestListBox.Visibility = Visibility.Visible;
            ParsedJsonListBox.Visibility = Visibility.Hidden;
            CountNumbertTxtBlock.Visibility = Visibility.Hidden;
            ParsedJsonTextBlock.Visibility = Visibility.Hidden;
            FilterResultsListbox.Visibility = Visibility.Hidden;
            CountFilterResultsTxtBlock.Visibility = Visibility.Hidden;
            CounterFilterResultsTxtblock.Visibility = Visibility.Hidden;
        }

        private void ShowOnlyFilterListbox()
        {
            ParsedJsonListBox.Visibility = Visibility.Hidden;
            CountNumbertTxtBlock.Visibility = Visibility.Hidden;
            ParsedJsonTextBlock.Visibility = Visibility.Hidden;
            TestListBox.Visibility = Visibility.Hidden;
            CountTestListBoxResultsTxtBlock.Visibility = Visibility.Hidden;
            CounterTestListboxResultsTxtblock.Visibility = Visibility.Hidden;
            FilterResultsListbox.Visibility = Visibility.Visible;
            CountFilterResultsTxtBlock.Visibility = Visibility.Visible;
            CounterFilterResultsTxtblock.Visibility = Visibility.Visible;
        }
        private void ShowOnlyParsedListbox()
        {
            CountNumbertTxtBlock.Visibility = Visibility.Visible;
            ParsedJsonTextBlock.Visibility = Visibility.Visible;
            ParsedJsonListBox.Visibility = Visibility.Visible;
            TestListBox.Visibility = Visibility.Hidden;
            CountTestListBoxResultsTxtBlock.Visibility = Visibility.Hidden;
            CounterTestListboxResultsTxtblock.Visibility = Visibility.Hidden;
            FilterResultsListbox.Visibility = Visibility.Hidden;
            CountFilterResultsTxtBlock.Visibility = Visibility.Hidden;
            CounterFilterResultsTxtblock.Visibility = Visibility.Hidden;
        }

        private void ButtonGeneral_Click(object sender, RoutedEventArgs e)
        {
            AddAlarmColumnNamesFilter();
            CountNumbertTxtBlock.Visibility = Visibility.Hidden;
            ParsedJsonTextBlock.Visibility = Visibility.Hidden;
            ClearFilterResultsButton.Visibility = Visibility.Visible;
            ClearFilterResultsButtonBorder.Visibility = Visibility.Visible;
            ClearSearchResultsButton.Visibility = Visibility.Visible;
            ClearSearchButtonBorder.Visibility = Visibility.Visible;

            if (FilterResultsListbox.Items.Count > 0)
            {
                FilterResultsListbox.Items.Clear();

            }
            else
            {
                CounterFilterResultsTxtblock.Text = 0.ToString();
            }

            if (TestListBox.Items.Count > 0)
            {

                foreach (string general in TestListBox.Items)




                    if (general.Contains("log in from") || general.Contains("has connected") || general.Contains("roams from") || general.Contains("was disconnected"))
                    {
                        FilterResultsListbox.Items.Add(general);

                        if (FilterResultsListbox.Items.IsEmpty)
                        {
                            CounterFilterResultsTxtblock.Text = 0.ToString();
                        }
                        CounterFilterResultsTxtblock.Text = FilterResultsListbox.Items.Count.ToString();

                    }

            }
            else if (ParsedJsonListBox.Items.Count > 0)
            {
                if (DownloadOneWebsiteAlarmsButton.Content == "Add one website alarms")
                {

                    FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                }

                if (DownloadOneWebsiteEventsButton.Content == "Add one website events")
                {
                    //FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                }
                if (SelectAlarmsDatesBeforeDownloadButton.Content == "Downloaded alarms!")
                {
                    FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                }
                if (SelectEventsDatesBeforeDownloadButton.Content == "Downloaded events!")
                {
                    FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                }
                if (DownloadAllWebsitesAlarmsButton.Content == "All alarms downloaded")
                {
                    FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                }
                if (DownloadAllWebsiteEventsButton.Content == "All events downloaded")
                {
                    FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                }
                foreach (string general in ParsedJsonListBox.Items)
                {




                    if (general.Contains("log in from") || general.Contains("has connected") || general.Contains("roams from") || general.Contains("was disconnected"))
                    {

                        FilterResultsListbox.Items.Add(general);
                        if (FilterResultsListbox.Items.IsEmpty)
                        {
                            CounterFilterResultsTxtblock.Text = 0.ToString();
                        }
                        CounterFilterResultsTxtblock.Text = FilterResultsListbox.Items.Count.ToString();


                    }

                }

            }
        }

        private void ButtonWarnings_Click(object sender, RoutedEventArgs e)
        {
            ClearSearchResultsButton.Visibility = Visibility.Visible;
            ClearSearchButtonBorder.Visibility = Visibility.Visible;
            ClearFilterResultsButton.Visibility = Visibility.Visible;
            ClearFilterResultsButtonBorder.Visibility = Visibility.Visible;
            CounterFilterResultsTxtblock.Text = 0.ToString();

            if (FilterResultsListbox.Items.Count > 0)
            {
                FilterResultsListbox.Items.Clear();
            }
            else
            {
                CounterFilterResultsTxtblock.Text = 0.ToString();
            }
            if (TestListBox.Items.Count > 0)
            {

                foreach (string warning in TestListBox.Items)




                    if (warning.Contains("was restarted"))
                    {

                        FilterResultsListbox.Items.Add(warning);

                        if (FilterResultsListbox.Items.IsEmpty)
                        {
                            CounterFilterResultsTxtblock.Text = 0.ToString();
                        }
                        CounterFilterResultsTxtblock.Text = FilterResultsListbox.Items.Count.ToString();


                    }

            }

            else if (ParsedJsonListBox.Items.Count > 0)
            {
                if (DownloadOneWebsiteAlarmsButton.Content == "Add one website alarms")
                {

                    //FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                }

                if (DownloadOneWebsiteEventsButton.Content == "Add one website events")
                {
                    //FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                }
                if (SelectAlarmsDatesBeforeDownloadButton.Content == "Downloaded alarms")
                {
                    //FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                }
                if (SelectEventsDatesBeforeDownloadButton.Content == "Downloaded evets!")
                {
                    //FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                }

                foreach (string warning in ParsedJsonListBox.Items)
                {




                    if (warning.Contains("was restarted"))
                    {

                        FilterResultsListbox.Items.Add(warning);
                        if (FilterResultsListbox.Items.IsEmpty)
                        {
                            CounterFilterResultsTxtblock.Text = 0.ToString();
                        }
                        CounterFilterResultsTxtblock.Text = FilterResultsListbox.Items.Count.ToString();


                    }

                }
            }

        }

        private void DownloadWebsitesEvents_Click(object sender, RoutedEventArgs e)
        {
            DownloadAllWebsiteEventsButton.Content = "All events downloaded";
            ParsedJsonListBox.Items.Add(SiteEventsColumnNames);
            DownloadAllEvents();

        }

        private void DownloadAllEvents()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            string sitesEventsJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonEvents.json");

            JToken parsedSitesJson = JToken.Parse(sitesEventsJson.ToString());
            parsedSitesJson.ToString(Newtonsoft.Json.Formatting.Indented);
            JsonTextBox.Text = sitesEventsJson.ToString();
            deserializeAllEventsJSON(JsonTextBox.Text);

            sw.Stop();
            TimeSpentDownloadAllSitesTextblock.Text = sw.Elapsed.ToString();
        }

        private void DownloadFilesBtn_Click(object sender, RoutedEventArgs e)
        {

            using (var output = new StreamWriter("C:\\Users\\koitko\\Desktop\\webpage html\\done\\allAlarms.html"))
            {

                foreach (var file in Directory.GetFiles("C:\\Users\\koitko\\Desktop\\webpage html\\AllAlarms", "*.html"))
                {
                    using (var input = new StreamReader(file))
                    {
                        output.WriteLine(input.ReadToEnd());



                    }
                }
                System.Windows.MessageBox.Show("Alarms Downloaded!");
            }
            using (var output = new StreamWriter("C:\\Users\\koitko\\Desktop\\webpage html\\done\\allEvents.html"))
            {

                foreach (var file in Directory.GetFiles("C:\\Users\\koitko\\Desktop\\webpage html\\AllEvents", "*.html"))
                {
                    using (var input = new StreamReader(file))
                    {
                        output.WriteLine(input.ReadToEnd());



                    }
                }
                System.Windows.MessageBox.Show("Events Downloaded!");
            }
        }





        private void ParseDownloadedAlarmsBtn_Click(object sender, RoutedEventArgs e)
        {
            string url = "C:\\Users\\koitko\\Desktop\\webpage html\\done\\allAlarms.html";
            WebClient webClient = new WebClient();
            string html = webClient.DownloadString(url);
            HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
            htmlDocument.LoadHtml(html);

            foreach (HtmlNode node in htmlDocument.DocumentNode.SelectNodes("//pre[@id='pre_output']"))
            {

                JsonTextBox.Text = node.InnerHtml;
                if (node.InnerHtml.Contains("admin"))
                {
                    deserializeEventsJSON(JsonTextBox.Text);
                }
                else
                {
                    deserializeAlarmsJSON(JsonTextBox.Text);
                }
            }

        }

        private void ParseDownloadedEventsBtn_Click(object sender, RoutedEventArgs e)
        {
            string url = "C:\\Users\\koitko\\Desktop\\webpage html\\done\\allEvents.html";
            WebClient webClient = new WebClient();
            string html = webClient.DownloadString(url);
            HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
            htmlDocument.LoadHtml(html);

            foreach (HtmlNode node in htmlDocument.DocumentNode.SelectNodes("//pre[@id='pre_output']"))
            {

                JsonTextBox.Text = node.InnerHtml;
                if (node.InnerHtml.Contains("admin"))
                {
                    deserializeEventsJSON(JsonTextBox.Text);
                }
                else
                {
                    deserializeAlarmsJSON(JsonTextBox.Text);
                }
            }
        }

        private void DownloadWebsitesAlarms_Click(object sender, RoutedEventArgs e)
        {
            DownloadAllWebsitesAlarmsButton.Content = "All alarms downloaded";
            ParsedJsonListBox.Items.Add(SiteAlarmsColumnNames);
            DownloadAllWebsitesAlarms();

        }
        private void DownloadAllWebsitesAlarms()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            string sitesEventsJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonAlarms.json");
            JToken parsedSitesJson = JToken.Parse(sitesEventsJson.ToString());
            parsedSitesJson.ToString(Newtonsoft.Json.Formatting.Indented);
            JsonTextBox.Text = sitesEventsJson.ToString();
            deserializeAllAlarmsJSON(JsonTextBox.Text);

            sw.Stop();
            TimeSpentDownloadAllSitesTextblock.Text = sw.Elapsed.ToString();

        }

        private void deserializeAllAlarmsJSON(string JSON)
        {
            int counter = 0;
            var newline = Environment.NewLine;
            string AllSiteNamesAlarms;
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctJsonSitesList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonSitesTextBox.Text, typeof(List<JsonSites>));
            List<JsonAlarmsList> correctJsonList = (List<JsonAlarmsList>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonAlarmsList>));
            List<JsonAlarmsList> sortedAlarmsJsonList = correctJsonList.OrderBy(o => o.datetime).ToList();


            foreach (JsonSites item in correctJsonSitesList)
            {

                foreach (JsonAlarmsList json in correctJsonList)
                {

                    if (json.site_id != item._id)
                    {
                        AllSiteNamesAlarms = null;
                    }
                    else if (json.site_id == item._id)
                    {

                        AllSiteNamesAlarms = item.desc;
                        var allAlarmsInfo =

                            ("Alarm id:  ") + json._id + "," + newline +
                            ("Access point:  ") + json.ap + "," + newline +
                            ("Access point name:  ") + json.ap_name + "," + newline +
                            ("Archived:  ") + json.archived + "," + newline +
                            ("Datetime:  ") + json.datetime + "," + newline +
                            ("Key:  ") + json.key + "," + newline +
                            ("Message:  ") + json.msg + "," + newline +
                            ("Site id:  ") + json.site_id + "," + newline +
                            ("Subsystem type:  ") + json.subsystem + "," + newline +
                            ("Time:  ") + json.time + "," + newline;


                        //listboxi oma

                        //formaat
                        String SiteName, AlarmId, AccessPoint, AccessPointName, Archived, DateTime, Key, Message, SiteId, SubsystemType, Time;
                        AlarmId = json._id + (" ");
                        AccessPoint = json.ap + (" ");
                        AccessPointName = json.ap_name + (" ");
                        Archived = json.archived + (" ");
                        DateTime = json.datetime + (" ");
                        Key = json.key + (" ");
                        Message = json.msg + (" ");
                        SiteId = json.site_id + (" ");
                        SiteName = AllSiteNamesAlarms;
                        SubsystemType = json.subsystem + (" ");
                        Time = json.time + (" ");

                        //lisab kindla formaadiga
                        ParsedJsonListBox.Items.Add(String.Format(SiteAlarmsData, AlarmId, AccessPoint, AccessPointName, Archived, DateTime, Key, Message, SiteId, SiteName, SubsystemType, Time));
                        counter++;
                    }

                    ShowOnlyParsedListbox();

                    ParsedJsonTextBlock.Visibility = Visibility.Visible;
                    CountNumbertTxtBlock.Visibility = Visibility.Visible;
                    CountNumbertTxtBlock.Text = ParsedJsonListBox.Items.Count.ToString();
                    if (ParsedJsonListBox.Items.Count == 0)
                    {
                        CountNumbertTxtBlock.Text = "0";
                    }
                }
            }
        }


        public void showWebsites()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var webClient = new WebClient();

            string sitesJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSites.json");

            JToken parsedSitesJson = JToken.Parse(sitesJson.ToString());
            parsedSitesJson.ToString(Newtonsoft.Json.Formatting.Indented);
            JsonSitesTextBox.Text = sitesJson.ToString();
            deserializeSitesJson(JsonSitesTextBox.Text);

            sw.Stop();
            TimeSpentDownloadAllSitesTextblock.Text = sw.Elapsed.ToString();
        }

        private void AllSitesCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void FindSelectedSiteButton_Click(object sender, RoutedEventArgs e)
        {
            ShowOnlyParsedListbox();

            CounterTestListboxResultsTxtblock.Text = 0.ToString();

            string selectedSite = AllSitesCombobox.SelectedItem.ToString();
            string correctSelectedSite = selectedSite.Substring(selectedSite.LastIndexOf(':') + 1);
            TestListBox.Visibility = Visibility.Visible;
            foreach (string selected in ParsedJsonListBox.Items)
            {
                if (selected.Contains(correctSelectedSite.ToString()))
                {

                    TestListBox.Items.Add(selected);
                    CounterTestListboxResultsTxtblock.Text = TestListBox.Items.Count.ToString(); ;
                }
            }
            if (TestListBox.Items.Count > 0)
            {
                FindSelectedSiteButton.Content = "Add selected site";
            }
        }

        private void ClearSearchResultsButton_Click(object sender, RoutedEventArgs e)
        {
            TestListBox.Items.Clear();
            CounterTestListboxResultsTxtblock.Text = 0.ToString();
            FindSelectedSiteButton.Content = "Find selected site";
            DownloadOneWebsiteAlarmsButton.Content = "Download one website alarms";
            DownloadOneWebsiteEventsButton.Content = "Download one website events";
            AllSitesCombobox.SelectedItem = null;

        }

        private void ClearSearchAndFilterResultsButton_Click(object sender, RoutedEventArgs e)
        {
            CounterFilterResultsTxtblock.Text = 0.ToString();
            CounterTestListboxResultsTxtblock.Text = 0.ToString();
            TestListBox.Items.Clear();
            FilterResultsListbox.Items.Clear();
            AllSitesCombobox.SelectedItem = null;
        }

        private void DownloadOneWebsiteEventsButton_Click(object sender, RoutedEventArgs e)
        {
            if (ParsedJsonListBox.Items.Count == 0)
            {
                AddEventsColumnNamesParsed();
            }
            else
            {
                DownloadOneSiteEvents();

            }
        }

        private void AddEventsColumnNamesParsed()
        {
            ParsedJsonListBox.Items.Add(SiteEventsColumnNames);
        }
        private void AddEventsColumnNamesFilter()
        {
            FilterResultsListbox.Items.Add(SiteEventsColumnNames);
        }

        private void DownloadOneSiteEvents()
        {

            ShowOnlyParsedListbox();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            ButtonGeneral.Visibility = Visibility.Hidden;
            var newline = Environment.NewLine;
            List<string> OneWebsiteAlarmsList = new List<string>();
            HtmlWeb hw = new HtmlWeb();
            if (AllSitesCombobox.SelectedItem == null)
            {
                System.Windows.MessageBox.Show("Please select site from combobox (All Websites)!");
            }
            else
            {
                String sitesJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSites.json");

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(sitesJson, typeof(List<JsonSites>));
                List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();

                foreach (JsonSites jsonSites in sortedSitesJsonList)
                {
                    string selectedSite = AllSitesCombobox.SelectedItem.ToString();

                    if (selectedSite.Contains(jsonSites._id))

                    {
                        DataContainer.selectedSiteIdAlarms = jsonSites._id;
                        String sitesEventsJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonEvents.json");
                        JsonTextBox.Text = sitesEventsJson;
                        deserializeEventsJSON(JsonTextBox.Text);

                    }


                }

                if (ParsedJsonListBox.Items.Count > 0)
                {
                    DownloadOneWebsiteEventsButton.Content = "Add one website events";
                }
                sw.Stop();
                TimeSpentDownloadAllSitesTextblock.Text = sw.Elapsed.ToString();
            }
        }

        private void DownloadOneWebsiteAlarmsButton_Click(object sender, RoutedEventArgs e)
        {
            if (ParsedJsonListBox.Items.Count == 0)
            {
                AddAlarmColumnNamesParsed();
            }
            else
            {
                DownloadOneWebsiteAlarms();

            }
        }

        private void AddAlarmColumnNamesParsed()
        {
            ParsedJsonListBox.Items.Add(SiteAlarmsColumnNames);
        }
        private void AddAlarmColumnNamesFilter()
        {
            FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
        }

        private void DownloadOneWebsiteAlarms()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            ButtonGeneral.Visibility = Visibility.Hidden;
            var newline = Environment.NewLine;
            List<string> OneWebsiteAlarmsList = new List<string>();
            HtmlWeb hw = new HtmlWeb();
            if (AllSitesCombobox.SelectedItem == null)
            {
                System.Windows.MessageBox.Show("Please select site from combobox (All Websites)!");
            }
            else
            {
                String sitesJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSites.json");

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(sitesJson, typeof(List<JsonSites>));
                List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();

                foreach (JsonSites jsonSites in sortedSitesJsonList)
                {
                    string selectedSite = AllSitesCombobox.SelectedItem.ToString();

                    if (selectedSite.Contains(jsonSites._id))

                    {
                        DataContainer.selectedSiteIdAlarms = jsonSites._id;
                        String sitesAlarmsJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonAlarms.json");
                        JsonTextBox.Text = sitesAlarmsJson;
                        deserializeAlarmsJSON(JsonTextBox.Text);

                    }


                }

                ButtonGeneral.Visibility = Visibility.Visible;
                if (ParsedJsonListBox.Items.Count > 0)
                {
                    DownloadOneWebsiteAlarmsButton.Content = "Add one website alarms";
                }
                sw.Stop();
                TimeSpentDownloadAllSitesTextblock.Text = sw.Elapsed.ToString();
            }
        }

        private void ClearFilterResultsButton_Click(object sender, RoutedEventArgs e)
        {

            FilterResultsListbox.Items.Clear();
            CounterFilterResultsTxtblock.Text = 0.ToString();
        }

        private void ClearTimeSpent_Click(object sender, RoutedEventArgs e)
        {
            TimeSpentDownloadAllSitesTextblock.Text = "00:00:00.000";
        }

        private void SortSitesByToday_Click(object sender, RoutedEventArgs e)
        {
            ShowOnlyTestlistbox();
            ClearFilterResultsButton.Visibility = Visibility.Hidden;
            ClearFilterResultsButtonBorder.Visibility = Visibility.Hidden;
            ClearSearchResultsButton.Visibility = Visibility.Visible;
            ClearSearchButtonBorder.Visibility = Visibility.Visible;
            ClearSearchButtonBorder.Visibility = Visibility.Visible;
            int counter = 0;

            CounterTestListboxResultsTxtblock.Text = 0.ToString();

            TestListBox.Items.Clear();
            var today = DateTime.Now.ToString("dd/MM/yyyy");

            if (DownloadAllWebsitesAlarmsButton.Content == "All alarms downloaded")
            {
                TestListBox.Items.Add(SiteAlarmsColumnNames);
            }
            if (DownloadAllWebsiteEventsButton.Content == "All events downloaded")
            {
                TestListBox.Items.Add(SiteEventsColumnNames);
            }
            if (DownloadOneWebsiteAlarmsButton.Content == "Add one website alarms")
            {

                TestListBox.Items.Add(SiteAlarmsColumnNames);
            }

            if (DownloadOneWebsiteEventsButton.Content == "Add one website events")
            {
                TestListBox.Items.Add(SiteEventsColumnNames);
            }
            foreach (string date in ParsedJsonListBox.Items)
            {
                if (date.Contains(today))
                {
                    if (DownloadOneWebsiteAlarmsButton.Content == "Add one website alarms")
                    {

                        FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                    }

                    if (DownloadOneWebsiteEventsButton.Content == "Add one website events")
                    {
                        FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                    }
                    TestListBox.Items.Add(date);
                    counter++;


                }
                if (TestListBox.Items.IsEmpty)
                {
                    CounterTestListboxResultsTxtblock.Text = 0.ToString();
                }
                CounterTestListboxResultsTxtblock.Text = counter.ToString();
            }
        }

        private void SortSitesByMonth_Click(object sender, RoutedEventArgs e)
        {
            ShowOnlyTestlistbox();
            ClearFilterResultsButton.Visibility = Visibility.Hidden;
            ClearFilterResultsButtonBorder.Visibility = Visibility.Hidden;

            ClearSearchResultsButton.Visibility = Visibility.Visible;

            ClearSearchButtonBorder.Visibility = Visibility.Visible;


            CounterTestListboxResultsTxtblock.Text = 0.ToString();
            int counter = 0;
            TestListBox.Items.Clear();
            var month = DateTime.Now.ToString("MM/yyyy");


            if (DownloadAllWebsitesAlarmsButton.Content == "All alarms downloaded")
            {
                TestListBox.Items.Add(SiteAlarmsColumnNames);
            }
            if (DownloadAllWebsiteEventsButton.Content == "All events downloaded")
            {
                TestListBox.Items.Add(SiteEventsColumnNames);
            }
            if (DownloadOneWebsiteAlarmsButton.Content == "Add one website alarms")
            {

                TestListBox.Items.Add(SiteAlarmsColumnNames);
            }
            if (SelectAlarmsDatesBeforeDownloadButton.Content == "Downloaded alarms!")
            {
                TestListBox.Items.Add(SiteAlarmsColumnNames);
            }
            if (DownloadOneWebsiteEventsButton.Content == "Add one website events")
            {
                TestListBox.Items.Add(SiteEventsColumnNames);
            }
            if (SelectEventsDatesBeforeDownloadButton.Content == "Downloaded events!")
            {
                TestListBox.Items.Add(SiteEventsColumnNames);
            }

            foreach (string date in ParsedJsonListBox.Items)
            {
                if (date.Contains(month))
                {
                    if (DownloadOneWebsiteAlarmsButton.Content == "Add one website alarms")
                    {

                        FilterResultsListbox.Items.Add(SiteAlarmsColumnNames);
                    }

                    if (DownloadOneWebsiteEventsButton.Content == "Add one website events")
                    {
                        FilterResultsListbox.Items.Add(SiteEventsColumnNames);
                    }
                    TestListBox.Items.Add(date);
                    counter++;


                }
                if (TestListBox.Items.IsEmpty)
                {
                    CounterTestListboxResultsTxtblock.Text = 0.ToString();
                }
                CounterTestListboxResultsTxtblock.Text = counter.ToString();
            }
        }

        private void SortSitesByYear_Click(object sender, RoutedEventArgs e)
        {

            ShowOnlyTestlistbox();
            ClearFilterResultsButton.Visibility = Visibility.Hidden;
            ClearFilterResultsButtonBorder.Visibility = Visibility.Hidden;
            ClearSearchResultsButton.Visibility = Visibility.Visible;
            ClearSearchButtonBorder.Visibility = Visibility.Visible;

            CounterTestListboxResultsTxtblock.Text = 0.ToString();
            int counter = 0;
            TestListBox.Items.Clear();
            var year = DateTime.Now.ToString("yyyy");

            if (DownloadAllWebsitesAlarmsButton.Content == "All alarms downloaded")
            {
                TestListBox.Items.Add(SiteAlarmsColumnNames);
            }
            if (SelectAlarmsDatesBeforeDownloadButton.Content == "Downloaded alarms!")
            {
                TestListBox.Items.Add(SiteAlarmsColumnNames);
            }
            if (SelectEventsDatesBeforeDownloadButton.Content == "Downloaded events!")
            {
                TestListBox.Items.Add(SiteEventsColumnNames);
            }
            if (DownloadAllWebsiteEventsButton.Content == "All events downloaded")
            {
                TestListBox.Items.Add(SiteEventsColumnNames);
            }
            if (DownloadOneWebsiteAlarmsButton.Content == "Add one website alarms")
            {

                TestListBox.Items.Add(SiteAlarmsColumnNames);
            }

            if (DownloadOneWebsiteEventsButton.Content == "Add one website events")
            {
                TestListBox.Items.Add(SiteEventsColumnNames);
            }
            foreach (string date in ParsedJsonListBox.Items)
            {

                if (date.Contains(year))
                {

                    TestListBox.Items.Add(date);
                    counter++;


                }
                if (TestListBox.Items.IsEmpty)
                {
                    CounterTestListboxResultsTxtblock.Text = 0.ToString();
                }
                CounterTestListboxResultsTxtblock.Text = counter.ToString();
            }
        }

        private void SortSitesByAll_Click(object sender, RoutedEventArgs e)
        {
            ShowOnlyTestlistbox();
            ClearFilterResultsButton.Visibility = Visibility.Hidden;
            ClearFilterResultsButtonBorder.Visibility = Visibility.Hidden;
            ClearSearchResultsButton.Visibility = Visibility.Visible;
            ClearSearchButtonBorder.Visibility = Visibility.Visible;

            int counter = 0;
            CounterTestListboxResultsTxtblock.Text = 0.ToString();

            TestListBox.Items.Clear();
            foreach (var allItem in ParsedJsonListBox.Items)
            {
                TestListBox.Items.Add(allItem);
                counter++;
                if (TestListBox.Items.IsEmpty)
                {
                    CounterTestListboxResultsTxtblock.Text = 0.ToString();
                }
            }
            CounterTestListboxResultsTxtblock.Text = counter.ToString();
        }

        private void SortSitesBy1Hour_Click(object sender, RoutedEventArgs e)
        {
            TestListBox.Items.Clear();
            ShowOnlyTestlistbox();
            ClearFilterResultsButton.Visibility = Visibility.Hidden;
            ClearFilterResultsButtonBorder.Visibility = Visibility.Hidden;
            ClearSearchResultsButton.Visibility = Visibility.Visible;
            ClearSearchButtonBorder.Visibility = Visibility.Visible;

            CounterTestListboxResultsTxtblock.Text = 0.ToString();

            int counter = 0;
            var hourNow = DateTime.Now;
            var hourPast = DateTime.Now.AddHours(-1);

            //uus versioon üksteise järel
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonTextBox.Text, typeof(List<JsonSites>));
            List<JsonAlarmsList> correctAlarmsJsonList = (List<JsonAlarmsList>)javaScriptSerializer.Deserialize(JsonTextBox.Text, typeof(List<JsonAlarmsList>));
            List<JsonEventsList> correctEventsJsonList = (List<JsonEventsList>)javaScriptSerializer.Deserialize(JsonTextBox.Text, typeof(List<JsonEventsList>));
            if (DownloadAllWebsitesAlarmsButton.Content == "All alarms downloaded")
            {
                TestListBox.Items.Add(SiteAlarmsColumnNames);
            }

            if (DownloadAllWebsiteEventsButton.Content == "All events downloaded")
            {
                TestListBox.Items.Add(SiteEventsColumnNames);
            }
            if (DownloadOneWebsiteAlarmsButton.Content == "Add one website alarms")
            {

                TestListBox.Items.Add(SiteAlarmsColumnNames);
            }

            if (DownloadOneWebsiteEventsButton.Content == "Add one website events")
            {
                TestListBox.Items.Add(SiteEventsColumnNames);
            }

            foreach (JsonSites jsonSites in correctSitesJsonList)
            {

                if (DataContainer.selectedSiteIdAlarms == jsonSites.name)

                {
                    DataContainer.selectedSiteIdAlarms = jsonSites._id;
                }
            }





            foreach (JsonAlarmsList jsonAlarms in correctAlarmsJsonList)
            {

                if (jsonAlarms.site_id == DataContainer.selectedSiteIdAlarms)
                {

                    if (jsonAlarms.datetime >= hourPast && jsonAlarms.datetime <= hourNow)
                    {

                        if (DownloadOneWebsiteAlarmsButton.Content == "Add one website alarms")
                        {

                            var allAlarms =

                                "," + jsonAlarms._id + "," +
                                jsonAlarms.ap + "," +
                                jsonAlarms.ap_name + "," +
                                jsonAlarms.archived + "," +
                                jsonAlarms.datetime + "," +
                                jsonAlarms.key + "," +
                                jsonAlarms.msg + "," +
                                jsonAlarms.site_id + "," +
                                jsonAlarms.subsystem + "," +
                                jsonAlarms.time + ",";
                            TestListBox.Items.Add(allAlarms);
                        }
                    }



                }
            }
            foreach (JsonEventsList jsonEvents in correctEventsJsonList)
            {
                if (jsonEvents.site_id == DataContainer.selectedSiteIdAlarms)
                {

                    if (jsonEvents.datetime >= hourPast && jsonEvents.datetime <= hourNow)
                    {

                        if (DownloadOneWebsiteEventsButton.Content == "Add one website events")
                        {
                            var allEvents =

                                "," + jsonEvents._id + "," +
                                jsonEvents.admin + "," +
                                jsonEvents.datetime + "," +
                                jsonEvents.ip + "," +
                                jsonEvents.is_admin + "," +
                                jsonEvents.key + "," +
                                jsonEvents.msg + "," +
                                jsonEvents.site_id + "," +
                                jsonEvents.subsystem + "," +
                                jsonEvents.time + ",";
                            TestListBox.Items.Add(allEvents);
                            counter++;
                        }
                    }
                }
            }


            if (TestListBox.Items.IsEmpty)
            {
                CounterTestListboxResultsTxtblock.Text = 0.ToString();
            }
            CounterTestListboxResultsTxtblock.Text = counter.ToString();
        }

        private void SelectedDatesButton_Click(object sender, RoutedEventArgs e)
        {
            //peaks saama, ennem valida kuupäevad, ning siis valib ära kõik mida vaja.
            if (StartDateSelected.Text == "" && EndDateSelected.Text == "")
            {
                System.Windows.MessageBox.Show("Please choose start and end date!");

            }
            else
            {
                var startDate = StartDateSelected.SelectedDate;
                var endDate = EndDateSelected.SelectedDate;

                foreach (string selectedDate in ParsedJsonListBox.Items)
                {
                    string dateFirstPart = selectedDate.Substring(selectedDate.IndexOf("Datetime:"));
                    string dateSecondPart = string.Concat(dateFirstPart.TakeWhile((c) => c != ','));
                    string correctDateTime = dateSecondPart.Remove(0, dateSecondPart.IndexOf(' ') + 1);

                    if (Convert.ToDateTime(correctDateTime) >= startDate || Convert.ToDateTime(correctDateTime) == startDate && Convert.ToDateTime(correctDateTime) <= endDate || Convert.ToDateTime(correctDateTime) == endDate)
                    {

                        TestListBox.Items.Add(selectedDate);

                    }
                }
                if (TestListBox.Items.IsEmpty)
                {
                    CounterTestListboxResultsTxtblock.Text = 0.ToString();
                }
                CounterTestListboxResultsTxtblock.Text = TestListBox.Items.Count.ToString();
            }

        }

        private void SelectedDatesButton_Copy_Click(object sender, RoutedEventArgs e)
        {
            StartDateSelected.Text = "";
            EndDateSelected.Text = "";
            SelectEventsDatesBeforeDownloadButton.Content = "Select events with date!";
            SelectAlarmsDatesBeforeDownloadButton.Content = "Select alarms with date!";
        }

        private void SelectAlarmsDatesBeforeDownloadButton_Click(object sender, RoutedEventArgs e)
        {
            ParsedJsonListBox.Items.Clear();
            ParsedJsonListBox.Items.Add(SiteAlarmsColumnNames);

            ShowOnlyParsedListbox();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            if (AllSitesCombobox.SelectedItem == null)
            {
                DownloadAllAlarmsDateTimeData();

            }

            else if (AllSitesCombobox.SelectedItem != null)
            {
                DownloadOneAlarmsDateTimeData();
            }

            SelectAlarmsDatesBeforeDownloadButton.Content = "Downloaded alarms!";
            ButtonGeneral.Visibility = Visibility.Visible;
            sw.Stop();
            TimeSpentDownloadAllSitesTextblock.Text = sw.Elapsed.ToString();
        }

        private void DownloadOneAlarmsDateTimeData()
        {
            ButtonGeneral.Visibility = Visibility.Hidden;
            var startDate = StartDateSelected.SelectedDate;
            var endDate = EndDateSelected.SelectedDate;
            DataContainer.startTime = startDate.Value;
            DataContainer.endTime = endDate.Value;
            if (StartDateSelected.Text == "" && EndDateSelected.Text == "" || endDate <= startDate)
            {
                ParsedJsonListBox.Items.Clear();
                CountNumbertTxtBlock.Text = 0.ToString();
                System.Windows.MessageBox.Show("Please choose correct start and end date!");

            }
            else
            {
                String sitesJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSites.json");
                String sitesAlarmsJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonAlarms.json");
                JsonTextBox.Text = sitesAlarmsJson;
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(sitesJson, typeof(List<JsonSites>));
                List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();
                List<JsonAlarmsList> correctSitesAlarmsJsonList = (List<JsonAlarmsList>)javaScriptSerializer.Deserialize(sitesAlarmsJson, typeof(List<JsonAlarmsList>));

                string selectedSite = AllSitesCombobox.SelectedItem.ToString();
                foreach (JsonSites jsonSites in sortedSitesJsonList)
                {

                    if (selectedSite.Contains(jsonSites._id))
                    {
                        DataContainer.selectedSiteIdAlarms = jsonSites._id;
                        deserializeAlarmsDatesJSON(JsonTextBox.Text);

                    }

                }
                SelectAlarmsDatesBeforeDownloadButton.Content = "Downloaded alarms!";


            }
        }

        private void DownloadAllAlarmsDateTimeData()
        {
            ParsedJsonListBox.Items.Clear();
            ParsedJsonListBox.Items.Add(SiteAlarmsColumnNames);
            CountNumbertTxtBlock.Text = 0.ToString();
            string AllSiteNamesAlarms;
            int counter = 0;

            ButtonGeneral.Visibility = Visibility.Hidden;
            var startDate = StartDateSelected.SelectedDate;
            var endDate = EndDateSelected.SelectedDate;
            if (StartDateSelected.Text == "" && EndDateSelected.Text == "" || endDate <= startDate)
            {

                ParsedJsonListBox.Items.Clear();
                CountNumbertTxtBlock.Text = 0.ToString();
                System.Windows.MessageBox.Show("Please choose correct start and end date!");

            }
            else
            {
                DataContainer.startTime = startDate.Value;
                DataContainer.endTime = endDate.Value;
                String sitesJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSites.json");
                String sitesAlarmsJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonAlarms.json");
                JsonTextBox.Text = sitesAlarmsJson;
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(sitesJson, typeof(List<JsonSites>));
                List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();
                List<JsonAlarmsList> correctSitesAlarmsJsonList = (List<JsonAlarmsList>)javaScriptSerializer.Deserialize(sitesAlarmsJson, typeof(List<JsonAlarmsList>));


                foreach (JsonAlarmsList alarms in correctSitesAlarmsJsonList)
                {
                    if (alarms.datetime >= DataContainer.startTime && alarms.datetime <= DataContainer.endTime)
                    {

                        foreach (JsonSites jsonSites in correctSitesJsonList)
                        {
                            if (alarms.site_id == jsonSites._id)
                            {
                                AllSiteNamesAlarms = jsonSites.desc;
                                String SiteName, AlarmId, AccessPoint, AccessPointName, Archived, DateTime, Key, Message, SiteId, SubsystemType, Time;

                                AlarmId = alarms._id + (" ");
                                AccessPoint = alarms.ap + (" ");
                                AccessPointName = alarms.ap_name + (" ");
                                Archived = alarms.archived + (" ");
                                DateTime = alarms.datetime + (" ");
                                Key = alarms.key + (" ");
                                Message = alarms.msg + (" ");
                                SiteId = alarms.site_id + (" ");
                                SiteName = AllSiteNamesAlarms;
                                SubsystemType = alarms.subsystem + (" ");
                                Time = alarms.time + (" ");

                                ParsedJsonListBox.Items.Add(String.Format(SiteAlarmsData, AlarmId, AccessPoint, AccessPointName, Archived, DateTime, Key, Message, SiteId, SiteName, SubsystemType, Time));
                                counter++;
                            }
                            ShowOnlyParsedListbox();

                            ParsedJsonTextBlock.Visibility = Visibility.Visible;

                            CountNumbertTxtBlock.Visibility = Visibility.Visible;
                            CountNumbertTxtBlock.Text = counter.ToString();
                            if (ParsedJsonListBox.Items.Count == 0)
                            {
                                CountNumbertTxtBlock.Text = "0";
                            }

                        }

                    }
                }


            }
        }

        private void deserializeAllAlarmsDatesJSON(string JSON)
        {
            //int counter = 0;
            var newline = Environment.NewLine;
            string AllSiteNamesAlarms;
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctJsonSitesList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonSitesTextBox.Text, typeof(List<JsonSites>));
            List<JsonAlarmsList> correctJsonList = (List<JsonAlarmsList>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonAlarmsList>));
            List<JsonAlarmsList> sortedAlarmsJsonList = correctJsonList.OrderBy(o => o.datetime).ToList();


            foreach (JsonSites item in correctJsonSitesList)
            {

                foreach (JsonAlarmsList json in sortedAlarmsJsonList)
                {

                    if (json.site_id != item._id)
                    {
                        AllSiteNamesAlarms = null;
                    }
                    else if (json.site_id == item._id)
                    {

                        AllSiteNamesAlarms = item.name;
                        if (json.datetime >= DataContainer.startTime && json.datetime <= DataContainer.endTime)
                        {
                            var allAlarmsInfo =

                            ("Alarm id:  ") + json._id + "," + newline +
                            ("Access point:  ") + json.ap + "," + newline +
                            ("Access point name:  ") + json.ap_name + "," + newline +
                            ("Archived:  ") + json.archived + "," + newline +
                            ("Datetime:  ") + json.datetime + "," + newline +
                            ("Key:  ") + json.key + "," + newline +
                            ("Message:  ") + json.msg + "," + newline +
                            ("Site id:  ") + json.site_id + "," + newline +
                            ("Subsystem type:  ") + json.subsystem + "," + newline +
                            ("Time:  ") + json.time + "," + newline;


                            //listboxi oma

                            //formaat
                            String SiteName, AlarmId, AccessPoint, AccessPointName, Archived, DateTime, Key, Message, SiteId, SubsystemType, Time;

                            AlarmId = json._id + (" ");
                            AccessPoint = json.ap + (" ");
                            AccessPointName = json.ap_name + (" ");
                            Archived = json.archived + (" ");
                            DateTime = json.datetime + (" ");
                            Key = json.key + (" ");
                            Message = json.msg + (" ");
                            SiteId = json.site_id + (" ");
                            SiteName = AllSiteNamesAlarms;
                            SubsystemType = json.subsystem + (" ");
                            Time = json.time + (" ");

                            ParsedJsonListBox.Items.Add(String.Format(SiteAlarmsData, AlarmId, AccessPoint, AccessPointName, Archived, DateTime, Key, Message, SiteId, SiteName, SubsystemType, Time));
                        }

                        ShowOnlyParsedListbox();
                        ParsedJsonTextBlock.Visibility = Visibility.Visible;
                        CountNumbertTxtBlock.Visibility = Visibility.Visible;
                        CountNumbertTxtBlock.Text = ParsedJsonListBox.Items.Count.ToString();
                        if (ParsedJsonListBox.Items.Count == 0)
                        {
                            CountNumbertTxtBlock.Text = "0";
                        }



                    }
                }

            }
        }



        private void deserializeDatesAlarmsJSON(string JSON)
        {

            ShowOnlyParsedListbox();
            //textboxi kaudu jsoni saamine
            var newline = Environment.NewLine;
            var startDate = StartDateSelected.SelectedDate;
            var endDate = EndDateSelected.SelectedDate;

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonAlarmsList> correctJsonList = (List<JsonAlarmsList>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonAlarmsList>));
            List<JsonAlarmsList> sortedAlarmsJsonList = correctJsonList.OrderBy(o => o.datetime).ToList();

            //sorted by datetime
            foreach (JsonAlarmsList json in sortedAlarmsJsonList)
            {
                string jsonDate = json.datetime.ToString();
                if (Convert.ToDateTime(jsonDate) >= startDate || Convert.ToDateTime(jsonDate) == startDate && Convert.ToDateTime(jsonDate) <= endDate || Convert.ToDateTime(jsonDate) == endDate)
                {

                    //listboxi oma
                    var allAlarmsInfo =

                        ("Alarm id:  ") + json._id + "," + newline +
                        ("Access point:  ") + json.ap + "," + newline +
                        ("Access point name:  ") + json.ap_name + "," + newline +
                        ("Archived:  ") + json.archived + "," + newline +
                        ("Datetime:  ") + json.datetime + "," + newline +
                        ("Key:  ") + json.key + "," + newline +
                        ("Message:  ") + json.msg + "," + newline +
                        ("Site id:  ") + json.site_id + "," + newline +
                        ("Subsystem type:  ") + json.subsystem + "," + newline +
                        ("Time:  ") + json.time + "," + newline;

                    String AlarmId, AccessPoint, AccessPointName, Archived, DateTime, Key, Message, SiteId, SubsystemType, Time;

                    AlarmId = json._id + (" ");
                    AccessPoint = json.ap + (" ");
                    AccessPointName = json.ap_name + (" ");
                    Archived = json.archived + (" ");
                    DateTime = json.datetime + (" ");
                    Key = json.key + (" ");
                    Message = json.msg + (" ");
                    SiteId = json.site_id + (" ");
                    SubsystemType = json.subsystem + (" ");
                    Time = json.time + (" ");

                    //lisab kindla formaadiga
                    ParsedJsonListBox.Items.Add(String.Format(SiteAlarmsData, AlarmId, AccessPoint, AccessPointName, Archived, DateTime, Key, Message, SiteId, SubsystemType, Time));
                    CountNumbertTxtBlock.Text = ParsedJsonListBox.Items.Count.ToString();
                    if (ParsedJsonListBox.Items.Count == 0)
                    {
                        CountNumbertTxtBlock.Text = "0";
                    }

                }

            }
        }

        private void SelectEventsDatesBeforeDownloadButton_Click(object sender, RoutedEventArgs e)
        {
            ParsedJsonListBox.Items.Clear();
            ParsedJsonListBox.Items.Add(SiteEventsColumnNames);

            ShowOnlyParsedListbox();
            Stopwatch sw = new Stopwatch();
            sw.Start();

            if (AllSitesCombobox.SelectedItem == null)
            {
                DownloadAllEventsDateTimeData();
            }

            else if (AllSitesCombobox.SelectedItem != null)
            {
                DownloadOneEventsDateTimedata();
            }

            ButtonGeneral.Visibility = Visibility.Visible;
            sw.Stop();
            TimeSpentDownloadAllSitesTextblock.Text = sw.Elapsed.ToString();
            SelectEventsDatesBeforeDownloadButton.Content = "Downloaded events!";
        }

        private void DownloadOneEventsDateTimedata()
        {
            var startDate = StartDateSelected.SelectedDate;
            var endDate = EndDateSelected.SelectedDate;
            DataContainer.startTime = startDate.Value;
            DataContainer.endTime = endDate.Value;
            if (StartDateSelected.Text == "" && EndDateSelected.Text == "" || endDate <= startDate)
            {
                ParsedJsonListBox.Items.Clear();
                CountNumbertTxtBlock.Text = 0.ToString();
                System.Windows.MessageBox.Show("Please choose correct start and end date!");

            }
            else
            {
                String sitesJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSites.json");
                String sitesEventsJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonEvents.json");
                JsonTextBox.Text = sitesEventsJson;
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(sitesJson, typeof(List<JsonSites>));
                List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();
                List<JsonEventsList> correctSitesAlarmsJsonList = (List<JsonEventsList>)javaScriptSerializer.Deserialize(sitesEventsJson, typeof(List<JsonEventsList>));

                string selectedSite = AllSitesCombobox.SelectedItem.ToString();
                foreach (JsonSites jsonSites in sortedSitesJsonList)
                {

                    if (selectedSite.Contains(jsonSites._id))
                    {
                        DataContainer.selectedSiteIdAlarms = jsonSites._id;
                        deserializeEventsDatesJSON(JsonTextBox.Text);

                    }
                }
            }
        }

        private void DownloadAllEventsDateTimeData()
        {
            string AllSiteNamesEvents;
            ParsedJsonListBox.Items.Clear();
            ParsedJsonListBox.Items.Add(SiteEventsColumnNames);
            CountNumbertTxtBlock.Text = 0.ToString();
            int counter = 0;
            var startDate = StartDateSelected.SelectedDate;
            var endDate = EndDateSelected.SelectedDate;
            if (StartDateSelected.Text == "" && EndDateSelected.Text == "" || endDate <= startDate)
            {
                ParsedJsonListBox.Items.Clear();
                CountNumbertTxtBlock.Text = 0.ToString();
                System.Windows.MessageBox.Show("Please choose correct start and end date!");

            }
            else
            {
                DataContainer.startTime = startDate.Value;
                DataContainer.endTime = endDate.Value;
                String sitesJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSites.json");
                String sitesEventsJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonEvents.json");
                JsonTextBox.Text = sitesEventsJson;
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(sitesJson, typeof(List<JsonSites>));
                List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();
                List<JsonEventsList> correctSitesEventsJsonList = (List<JsonEventsList>)javaScriptSerializer.Deserialize(sitesEventsJson, typeof(List<JsonEventsList>));

                foreach (JsonEventsList json in correctSitesEventsJsonList)
                {
                    if (json.datetime >= DataContainer.startTime && json.datetime <= DataContainer.endTime)
                    {

                        foreach (JsonSites jsonSites in sortedSitesJsonList)
                        {
                            if (json.site_id == jsonSites._id)
                            {
                                AllSiteNamesEvents = jsonSites.desc;

                                String EventId, Admin, Datetime, Ip, IsAdmin, Key, Message, SiteId, SiteName, SubsystemType, Time;

                                EventId = json._id + (" ");
                                Admin = json.admin + (" ");
                                Datetime = json.datetime + (" ");
                                Ip = json.ip + (" ");
                                IsAdmin = json.is_admin + (" ");
                                Key = json.key + (" ");
                                Message = json.msg + (" ");
                                SiteId = json.site_id + (" ");
                                SiteName = AllSiteNamesEvents;
                                SubsystemType = json.subsystem + (" ");
                                Time = json.time + (" ");

                                //lisab kindla formaadiga
                                ParsedJsonListBox.Items.Add(String.Format(SiteEventsData, EventId, Admin, Datetime, Ip, IsAdmin, Key, Message, SiteId, SiteName, SubsystemType, Time));
                                counter++;
                            }
                            ParsedJsonListBox.Visibility = Visibility.Visible;
                            CountNumbertTxtBlock.Text = counter.ToString();
                            if (ParsedJsonListBox.Items.Count == 0)
                            {
                                CountNumbertTxtBlock.Text = "0";
                            }


                        }


                    }
                }

            }
        }

        private void deserializeEventsAllDatesJSON(string JSON)
        {

            //textboxi kaudu jsoni saamine
            var newline = Environment.NewLine;

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctJsonSitesList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonSitesTextBox.Text, typeof(List<JsonSites>));
            List<JsonEventsList> correctJsonList = (List<JsonEventsList>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonEventsList>));
            List<JsonEventsList> sortedEventsJsonList = correctJsonList.OrderBy(o => o.datetime).ToList();

            foreach (JsonSites item in correctJsonSitesList)
            {

                foreach (JsonEventsList json in sortedEventsJsonList)
                {
                    if (json.site_id != item._id)
                    {

                    }
                    else if (json.site_id == item._id)
                    {
                        DataContainer.siteName = item.desc;
                    }
                    if (json.datetime >= DataContainer.startTime && json.datetime <= DataContainer.endTime)
                    {

                        //listboxi oma
                        var allEventsInfo =

                                    ("Event id:  ") + json._id + "," + newline +
                                    ("Admin:  ") + json.admin + "," + newline +
                                    ("Datetime:  ") + json.datetime + "," + newline +
                                    ("Ip:  ") + json.ip + "," + newline +
                                    ("Is admin:  ") + json.is_admin + "," + newline +
                                    ("Key:  ") + json.key + "," + newline +
                                    ("Message:  ") + json.msg + "," + newline +
                                    ("Site id:  ") + json.site_id + "," + newline +
                                    ("Subsystem type:  ") + json.subsystem + "," + newline +
                                    ("Time:  ") + json.time + "," + newline;

                        String EventId, Admin, Datetime, Ip, IsAdmin, Key, Message, SiteId, SiteName, SubsystemType, Time;

                        EventId = json._id + (" ");
                        Admin = json.admin + (" ");
                        Datetime = json.datetime + (" ");
                        Ip = json.ip + (" ");
                        IsAdmin = json.is_admin + (" ");
                        Key = json.key + (" ");
                        Message = json.msg + (" ");
                        SiteId = json.site_id + (" ");
                        SiteName = DataContainer.siteName;
                        SubsystemType = json.subsystem + (" ");
                        Time = json.time + (" ");

                        //lisab kindla formaadiga
                        ParsedJsonListBox.Items.Add(String.Format(SiteEventsData, EventId, Admin, Datetime, Ip, IsAdmin, Key, Message, SiteId, SiteName, SubsystemType, Time));
                        ParsedJsonListBox.Visibility = Visibility.Visible;


                        CountNumbertTxtBlock.Text = ParsedJsonListBox.Items.Count.ToString();
                        if (ParsedJsonListBox.Items.Count == 0)
                        {
                            CountNumbertTxtBlock.Text = "0";
                        }

                    }

                }

            }


        }

        private void deserializeDatesEventsJSON(string JSON)
        {
            ShowOnlyParsedListbox();
            //textboxi kaudu jsoni saamine
            var newline = Environment.NewLine;
            var startDate = StartDateSelected.SelectedDate;
            var endDate = EndDateSelected.SelectedDate;

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonEventsList> correctJsonList = (List<JsonEventsList>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonEventsList>));
            List<JsonEventsList> sortedEventsJsonList = correctJsonList.OrderBy(o => o.datetime).ToList();

            foreach (JsonEventsList json in sortedEventsJsonList)
            {
                string jsonDate = json.datetime.ToString();
                if (Convert.ToDateTime(jsonDate) >= startDate || Convert.ToDateTime(jsonDate) == startDate && Convert.ToDateTime(jsonDate) <= endDate || Convert.ToDateTime(jsonDate) == endDate)
                {

                    //listboxi oma
                    var allEventsInfo =

                    ("Event id:  ") + json._id + "," + newline +
                    ("Admin:  ") + json.admin + "," + newline +
                    ("Datetime:  ") + json.datetime + "," + newline +
                    ("Ip:  ") + json.ip + "," + newline +
                    ("Is admin:  ") + json.is_admin + "," + newline +
                    ("Key:  ") + json.key + "," + newline +
                    ("Message:  ") + json.msg + "," + newline +
                    ("Site id:  ") + json.site_id + "," + newline +
                    ("Subsystem type:  ") + json.subsystem + "," + newline +
                    ("Time:  ") + json.time + "," + newline;

                    String EventId, Admin, Datetime, Ip, IsAdmin, Key, Message, SiteId, SubsystemType, Time;

                    EventId = json._id + (" ");
                    Admin = json.admin + (" ");
                    Datetime = json.datetime + (" ");
                    Ip = json.ip + (" ");
                    IsAdmin = json.is_admin + (" ");
                    Key = json.key + (" ");
                    Message = json.msg + (" ");
                    SiteId = json.site_id + (" ");
                    SubsystemType = json.subsystem + (" ");
                    Time = json.time + (" ");

                    //lisab kindla formaadiga
                    ParsedJsonListBox.Items.Add(String.Format(SiteEventsData, EventId, Admin, Datetime, Ip, IsAdmin, Key, Message, SiteId, SubsystemType, Time));

                    CountNumbertTxtBlock.Text = ParsedJsonListBox.Items.Count.ToString();
                    if (ParsedJsonListBox.Items.Count == 0)
                    {
                        CountNumbertTxtBlock.Text = "0";
                    }

                }

            }
        }
    }
}