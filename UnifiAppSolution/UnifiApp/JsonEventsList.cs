﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnifiApp
{
    class JsonEventsList
    {
    //     {
    //    "_id": "5a8d6a32e082312dcf3e3f60",
    //    "admin": "Admin[koitadmin]",
    //    "datetime": "2018-02-21T12:46:42Z",
    //    "ip": "194.126.117.170",
    //    "is_admin": true,
    //    "key": "EVT_AD_Login",
    //    "msg": "Admin[koitadmin] log in from 194.126.117.170",
    //    "site_id": "57ff4b72f5725c4eddf77ed8",
    //    "subsystem": "",
    //    "time": 1519217202561
    //}


    public string _id { get; set; }
        public string admin { get; set; }
        public DateTime datetime { get; set; }
        public string ip { get; set; }
        public bool is_admin { get; set; }
        public string key { get; set; }
        public string msg { get; set; }
        public string site_id { get; set; }
        public string subsystem { get; set; }
        //public long time { get; set; }
        public string time { get;  set; }
        }

    }

