﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnifiApp
{
    /// <summary>
    /// Interaction logic for SummerTest.xaml
    /// </summary>
    public partial class SummerTest : Page
    {
        public SummerTest()
        {
            InitializeComponent();
        }
        //tõmbab kõigi saitide JSONI
        private void Download_Click(object sender, RoutedEventArgs e)
        {
            WebClient webClientAlarmsList = new WebClient();
            string allSites = webClientAlarmsList.DownloadString("http://localhost:82/UNIFI/index.php?action=list_sites&output_format=json");
            SitesJson.Text = allSites;
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(SitesJson.Text, typeof(List<JsonSites>));

            foreach (JsonSites existingSite in correctSitesJsonList)
            {
                //kõikide saitide alarmid
                string SiteId = existingSite.name;
                string SiteName = existingSite.desc;
                WebClient webClient = new WebClient();
                //tõmbab alla
                string SitesAlarms = webClient.DownloadString("http://localhost:82/UNIFI/index.php?" + "site_id=" + SiteId + "&" + "site_name=" + SiteName + "&" + "action=list_alarms&output_format=json");
                SitesJsonListbox.Items.Add(SitesAlarms);
            }
        }

        //Kõikide saitide tõmbamise URLID
        private void Urls_Click(object sender, RoutedEventArgs e)
        {
            WebClient webClientAlarmsList = new WebClient();
            string allSites = webClientAlarmsList.DownloadString("http://localhost:82/UNIFI/index.php?action=list_sites&output_format=json");
            SitesUrls.Text = allSites;
            List<string> OneWebsiteAlarmsList = new List<string>();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(SitesUrls.Text, typeof(List<JsonSites>));

            foreach (JsonSites existingSite in correctSitesJsonList)
            {
                string SiteId = existingSite.name;
                string SiteName = existingSite.desc;
                string SitesAlarms = "http://localhost:82/UNIFI/index.php?" + "site_id=" + SiteId + "&" + "site_name=" + SiteName + "&" + "action=list_alarms&output_format=json";
                OneWebsiteAlarmsList.Add(SitesAlarms);

            }
            foreach (var correct in OneWebsiteAlarmsList)
            {
                string encodedJsonWebsiteEvents =
                        correct.Replace(" ", "%20");
                SitesUrlListbox.Items.Add(encodedJsonWebsiteEvents);
            }


        }

        private void ButtonAll_Click(object sender, RoutedEventArgs e)
        {
            //tõmbab site jsoni alla
            WebClient webClientAlarmsList = new WebClient();
            string allSites = webClientAlarmsList.DownloadString("http://localhost:82/UNIFI/index.php?action=list_sites&output_format=json");
            JsonAll.Text = allSites;
            List<string> OneWebsiteAlarmsList = new List<string>();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(JsonAll.Text, typeof(List<JsonSites>));
            //URLID
            foreach (JsonSites existingSite in correctSitesJsonList)
            {
                string SiteId = existingSite.name;
                string SiteName = existingSite.desc;
                string SitesAlarms = "http://localhost:82/UNIFI/index.php?" + "site_id=" + SiteId + "&" + "site_name=" + SiteName + "&" + "action=list_alarms&output_format=json";
                OneWebsiteAlarmsList.Add(SitesAlarms);

            }
            foreach (var correct in OneWebsiteAlarmsList)
            {
                //Asendus
                string encodedJsonWebsiteEvents =
                        correct.Replace(" ", "%20");
                //SitesUrlListbox.Items.Add(encodedJsonWebsiteEvents);
                string correctEncodedJsonWebsiteAlarms = webClientAlarmsList.DownloadString(encodedJsonWebsiteEvents);
                JsonAllSites.Text = correctEncodedJsonWebsiteAlarms;
                Deserialize(JsonAllSites.Text);
            }
        }

        private void Deserialize(string Json)
        {
            var newline = Environment.NewLine;

            JavaScriptSerializer javaScript = new JavaScriptSerializer();
            List<JsonAlarmsList> correctJsonList = (List<JsonAlarmsList>)javaScript.Deserialize(Json, typeof(List<JsonAlarmsList>));
            List<JsonAlarmsList> sortedAlarmsJsonList = correctJsonList.OrderBy(o => o.datetime).ToList();


            //Listboxi lisamine
            foreach (JsonAlarmsList json in correctJsonList)
            {
                //listboxi oma
                var allAlarmsInfo =

                    ("Alarm id:  ") + json._id + "," + newline +
                    ("Access point:  ") + json.ap + "," + newline +
                    ("Access point name:  ") + json.ap_name + "," + newline +
                    ("Archived:  ") + json.archived + "," + newline +
                    ("Datetime:  ") + json.datetime + "," + newline +
                    ("Key:  ") + json.key + "," + newline +
                    ("Message:  ") + json.msg + "," + newline +
                    ("Site id:  ") + json.site_id + "," + newline +
                    ("Subsystem type:  ") + json.subsystem + "," + newline +
                    ("Time:  ") + json.time + "," + newline;
                SitesJsonAllListbox.Items.Add(allAlarmsInfo);
            }
        }
    }
}
