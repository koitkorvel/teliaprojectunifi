﻿using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnifiApp
{
    /// <summary>
    /// Interaction logic for SitesOverview.xaml
    /// </summary>
    public partial class SitesOverview : Page
    {
        private int wlanActiveCounter;
        private int correctwlanCounter;
        private int correctActiveWlanCounter;
        private int correctInActiveWlanCounter;

        public SitesOverview()
        {
            InitializeComponent();
            SitesOverviewJsonTextBox.Visibility = Visibility.Hidden;
            SitesListTextBox.Visibility = Visibility.Hidden;
            SitesList.Visibility = Visibility.Hidden;
            ShowsNavigationUI = true;
            var webClient = new WebClient();

            string sitesJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\JsonSites.json");
            SitesListTextBox.Text = sitesJson;
            deserializeSitesJson(SitesListTextBox.Text);
        }

        private void GetAllSitesOverviewButton_Click(object sender, RoutedEventArgs e)
        {

        }
        private String SiteOverviewColumnNames = ",Site id:,Site name:,Device id:,Wlan ip:,Mac aadress:,";
        private String SiteOverviewData = ",{0:0},{1:0},{2:0},{3:0},{4,0},";
        public void deserializeSitesJson(string JSON)
        {

            //textboxi kaudu jsoni saamine
            var newline = Environment.NewLine;

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonSites>));
            List<JsonSites> sortedSitesJsonList = correctSitesJsonList.OrderBy(o => o.desc).ToList();


            int sitesCounter = 0;



            foreach (JsonSites json in sortedSitesJsonList)
            {
                //comboboxi oma
                var allSitesInfo =
                    ("Id:  ") + json._id + "," + newline +
                    ("Description:  ") + json.desc + "," + newline +
                    ("Site name:  ") + json.name + "," + newline +
                    ("Role:  ") + json.role + "," + newline;

                sitesCounter++;
                SitesList.AppendText(allSitesInfo + newline);

            }
        }
        public void deserializeSitesOverviewJson(string JSON)
        {

            var newline = Environment.NewLine;
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            List<JsonSitesDevicesList> correctSitesOverviewJsonList = (List<JsonSitesDevicesList>)javaScriptSerializer.Deserialize(JSON, typeof(List<JsonSitesDevicesList>));
            List<JsonSites> correctSitesJsonList = (List<JsonSites>)javaScriptSerializer.Deserialize(SitesListTextBox.Text, typeof(List<JsonSites>));
            foreach (JsonSitesDevicesList json in correctSitesOverviewJsonList)
            {
                foreach (JsonSites sites in correctSitesJsonList)
                {
                    //Lisab site ID ja site name kokku. Jsoni sai SitesListTextBox.Text abil.
                    Dictionary<string, string> values = new Dictionary<string, string>();
                    values.Add(sites._id, sites.desc);
                    int correctInActiveWlanCounter = correctwlanCounter - correctActiveWlanCounter;
                    //kui values sisaldab õiget id, siis teeb edasi. Ehk kui leiab sobiva saidi, mida oodatakse, siis lisab koos andmetega listboxi koos õige formaadiga
                    if (values.ContainsKey(json.site_id))
                    {
                        correctwlanCounter++;

                        if (SitesOverviewJsonTextBox.Text.Contains("connect_request_ip"))
                        {
                            correctActiveWlanCounter++;
                        }
                        else if (!SitesOverviewJsonTextBox.Text.Contains("connect_request_ip"))
                        {
                            correctActiveWlanCounter = 0;
                        }
                        var allSitesOverviewInfo =
                           "Site id: " + json.site_id + newline +
                           "Site name: " + sites.desc + newline +
                           "Device id: " + json._id + newline +
                           "Wlan ip: " + json.ip + newline +
                           "Mac aadress: " + json.ethernet_table.mac;

                        String SiteId, SiteName, DeviceId, WlanIp, MacAadress;
                        SiteId = json.site_id + (" ");
                        SiteName = sites.desc + (" ");
                        DeviceId = json._id + (" ");
                        WlanIp = json.ip + (" ");
                        MacAadress = json.ethernet_table.mac + (" ");
                        SitesOverviewListbox.Items.Add(String.Format(SiteOverviewData, SiteId, SiteName, DeviceId, WlanIp, MacAadress));
                    }

                }

            }
        }

        private void GetSitesOverview_Click(object sender, RoutedEventArgs e)
        {
            SitesOverviewListbox.Items.Add(SiteOverviewColumnNames);
            List<string> WebsiteOverviewList = new List<string>();
            var newline = Environment.NewLine;
            string sitesDevicesJson = File.ReadAllText(@"D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\UnifiApp\testDataJson\MovedSiteDevices.json");
            SitesOverviewJsonTextBox.Text = sitesDevicesJson;
            deserializeSitesOverviewJson(SitesOverviewJsonTextBox.Text);
            
        }

        private void ExportCsvOverviewButton_Click(object sender, RoutedEventArgs e)
        {
            if (SitesOverviewListbox.Items.Count > 0)
            {

                using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "CSV|*.csv", ValidateNames = true })
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        using (StreamWriter file = new StreamWriter(new FileStream(sfd.FileName, FileMode.Create), Encoding.UTF8))
                        {
                            StringBuilder csv = new StringBuilder();

                            foreach (string item in SitesOverviewListbox.Items)
                            {
                                string correctItem = item.Replace("  ", "|");
                                file.WriteLine(correctItem);
                            }
                            file.Close();
                        }
                        System.Windows.MessageBox.Show("All data has been exported to CSV file!");
                    }
            }
            else
            {
                System.Windows.MessageBox.Show("Please select sites overview data!");
            }
        }
    }
}