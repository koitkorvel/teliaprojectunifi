﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnifiApp
{



}
public class JsonSitesHealth
{
    public string _id { get; set; }
    public string desc { get; set; }
    public Health healthLan { get; set; }
    public Health healthWlan { get; set; }
    public string name { get; set; }
    public int num_new_alarms { get; set; }
    public string attr_hidden_id { get; set; }
    public bool attr_no_delete { get; set; }
}

public class Health
{
    public int num_adopted { get; set; }
    public int num_ap { get; set; }
    public int num_disabled { get; set; }
    public int num_disconnected { get; set; }
    public int num_guest { get; set; }
    public int num_pending { get; set; }
    public int num_user { get; set; }
    public int rx_bytesr { get; set; }
    public string status { get; set; }
    public string subsystem { get; set; }
    public int tx_bytesr { get; set; }
    public int num_gw { get; set; }
    public int num_sw { get; set; }
}



