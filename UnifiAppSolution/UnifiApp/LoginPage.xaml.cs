﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevOne.Security.Cryptography.BCrypt;

namespace UnifiApp
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        private string correctpassword = string.Empty;

        public LoginPage()
        {
            InitializeComponent();
            ShowsNavigationUI = true;
        }

        public void createNewAccountBtn_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("CreatingNewAccount.xaml", UriKind.Relative));
        }

        public void forgotPassowrdBtn_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("RecoverPassword.xaml", UriKind.Relative));
        }

        private void loginBtn_Click(object sender, RoutedEventArgs e)
        {
            //siia vaja teha kasutaja andmed jne. praegu navigate lihtsalt Dashboard.
            SqlConnection connection = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;AttachDbFilename=D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\Database\UnifiAppDatabase.mdf;Initial Catalog=UnifiAppDatabase;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            string username = loggingUsernameTxtBox.Text;
            string password = loggingPasswordBox.Password;

            string passwordHash = String.Empty;
            using (SqlCommand cmd = new SqlCommand("SELECT Username, Password FROM dbo.Users WHERE Username = @Username", connection))
            {
                cmd.Parameters.AddWithValue("@Username", username);
                cmd.Connection = connection;
                connection.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.Read())
                    {

                        passwordHash = sdr["Password"].ToString();
                        bool correct = BCrypt.Net.BCrypt.Verify(password, passwordHash);

                        if (correct)
                        {
                            DataContainer.username = username;
                            NavigationService.Navigate(new Uri("DashBoard.xaml", UriKind.Relative));
                        }
                        else
                        {
                            MessageBox.Show("Check your password!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("User with this username does not exist!");
                    }
                }
            } 
        }

        private void easyLoginBtn_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("DashBoard.xaml", UriKind.Relative));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("DashBoard.xaml", UriKind.Relative));
        }
    }
}
