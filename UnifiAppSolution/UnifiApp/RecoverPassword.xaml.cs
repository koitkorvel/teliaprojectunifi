﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnifiApp
{
    /// <summary>
    /// Interaction logic for RecoverPassword.xaml
    /// </summary>
    public partial class RecoverPassword : Page
    {
        private string username = string.Empty;
        private string password = string.Empty;
        private string email = string.Empty;

        public RecoverPassword()
        {
            InitializeComponent();
            ShowsNavigationUI = true;
        }

        private void RecoverPasswordEmailSend_Click(object sender, RoutedEventArgs e)
        {

            using (SqlConnection con = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;AttachDbFilename=D:\Kool\TeliaProject\teliaprojectunifi\UnifiAppSolution\Database\UnifiAppDatabase.mdf;Initial Catalog=UnifiAppDatabase;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT Username, Password, Email FROM dbo.Users WHERE Email = @Email"))
                {
                    cmd.Parameters.AddWithValue("@Email", RecoverPasswordEmailTextbox.Text.Trim());
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.Read())
                        {
                            username = sdr["Username"].ToString();
                            password = sdr["Password"].ToString();
                            email = sdr["Email"].ToString();
                        }
                    }
                    con.Close();
                }
            }
            //uus
            if (RecoverPasswordEmailTextbox.Text == "")
            {
                MessageBox.Show("Please enter your Telia email, to recover password!");
            }
            else if (username == "" && password == "" && email == "")
            {
                MessageBox.Show("User doesn´t exist, but you can create your own user!");
                NavigationService.Navigate(new Uri("LoginPage.xaml", UriKind.Relative));
            }
            else
            {
                Guid guidvalue = Guid.NewGuid();
                MD5 mD5 = MD5.Create();
                Guid hashed = new Guid(mD5.ComputeHash(guidvalue.ToByteArray()));
                DataContainer.confirmationCode = hashed.ToString();
                DataContainer.username = username;

                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Port = 587;
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.EnableSsl = true;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new System.Net.NetworkCredential("unifitestingapp@gmail.com", "Triikraud123");

                System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();

                mailMessage.From = new MailAddress("unifitestingapp@gmail.com");
                mailMessage.To.Add(RecoverPasswordEmailTextbox.Text);
                mailMessage.Subject = "Password Recovery";
                mailMessage.Body = "Hello " + username + "!" +
                                    System.Environment.NewLine +
                                    "Confirmation code is: " + hashed;


                MessageBox.Show("Password recovery email is sent to: " + RecoverPasswordEmailTextbox.Text);
                smtpClient.Send(mailMessage);

                NavigationService.Navigate(new Uri("ForgotPasswordRecovery.xaml", UriKind.Relative));

            }

        }
    }
}
